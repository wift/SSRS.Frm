﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.Formula.Functions;
using SSRS.PlugIns.Base.SystemModule;

namespace SSRS.Application.Web.Areas.SystemPlugIns.Controllers
{
    [Area("SystemPlugIns")]
    public class ModuleController : Controller
    {
        private ModuleIBLL moduleIBLL = null;
        public ModuleController( ModuleIBLL moduleLazy   )
        {
            this.moduleIBLL = moduleLazy;
        }
        public IActionResult Index()
        {

            moduleIBLL.GetModuleTree();
            return View();
        }
    }
}