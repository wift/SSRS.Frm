﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SSRS.Core.Application.Web;
using SSRS.Core.Application.Web.Controllers;
using SSRS.PlugIns.Base.AuthorizeModule;
using SSRS.PlugIns.Base.SystemModule;
using SSRS.PlugIns.Organization;
using SSRS.Util;
using SSRS.Util.Operat;

namespace SSRS.Application.Web.Controllers
{
    [HandlerLogin(FilterMode.Ignore)]
    public class LoginController : BaseController
    {
        #region 构造函数    

        public UserIBLL Userbll { get; set; }

        /// <summary>
        /// 登陆界面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Default()
        {
            return RedirectToAction("Index", "Login");
        }

        /// <summary>
        /// 登陆界面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }

        /// <summary>
        /// 安全退出
        /// </summary>
        /// <returns></returns>
        [HttpPost]
         
        public ActionResult OutLogin()
        {
            var userInfo = LoginUserInfo.Get();
            LogEntity logEntity = new LogEntity();
            logEntity.F_CategoryId = 1;
            logEntity.F_OperateTypeId = ((int)OperationType.Exit).ToString();
            logEntity.F_OperateType = EnumAttribute.GetDescription(OperationType.Exit);
            logEntity.F_OperateAccount = userInfo.account + "(" + userInfo.realName + ")";
            logEntity.F_OperateUserId = userInfo.userId;
            logEntity.F_ExecuteResult = 1;
            logEntity.F_ExecuteResultJson = "退出系统";
            logEntity.F_Module = Config.GetValue("SoftName");
            logEntity.WriteLog();
            HttpContext.Session.Clear();  
                                  //清除当前浏览器所有Session
            OperatorHelper.Instance.EmptyCurrent();
            return Success("退出系统");
        }



        /// <summary>
        /// 登录验证
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="verifycode">验证码</param>
        /// <returns></returns>
        [HttpPost] 
        public ActionResult CheckLogin(string username, string password, string verifycode)
        {

            int error = OperatorHelper.Instance.GetCurrentErrorNum();
            if (error >= 3)
            {
                #region 验证码验证
                verifycode = Md5Helper.Encrypt(verifycode.ToLower(), 16);
                if (Session.GetString("session_verifycode").IsEmpty() || verifycode != Session.GetString("session_verifycode"))
                {
                    return Fail("验证码错误");
                }
                #endregion
            }

            #region 内部账户验证
            UserEntity userEntity = Userbll.CheckLogin(username, password);

            #region 写入日志
            LogEntity logEntity = new LogEntity();
            logEntity.F_CategoryId = 1;
            logEntity.F_OperateTypeId = ((int)OperationType.Login).ToString();
            logEntity.F_OperateType = EnumAttribute.GetDescription(OperationType.Login);
            logEntity.F_OperateAccount = username + "(" + userEntity.F_RealName + ")";
            logEntity.F_OperateUserId = !string.IsNullOrEmpty(userEntity.F_UserId) ? userEntity.F_UserId : username;
            logEntity.F_Module = Config.GetValue("SoftName");
            #endregion

            if (!userEntity.LoginOk)//登录失败
            {
                //写入日志
                logEntity.F_ExecuteResult = 0;
                logEntity.F_ExecuteResultJson = "登录失败:" + userEntity.LoginMsg;
                logEntity.WriteLog();
                int num = OperatorHelper.Instance.AddCurrentErrorNum();
                return Fail(userEntity.LoginMsg, num);
            }
            else
            {
                OperatorHelper.Instance.AddLoginUser(userEntity.F_Account, "SSRS_ADMS_1.0_PC", null);//写入缓存信息
                //写入日志
                logEntity.F_ExecuteResult = 1;
                logEntity.F_ExecuteResultJson = "登录成功";
                logEntity.WriteLog();
                OperatorHelper.Instance.ClearCurrentErrorNum();
                return Success("登录成功");
            }
            #endregion
        }


        /// <summary>
        /// 生成验证码
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult VerifyCode()
        {
            return File(new VerifyCode().GetVerifyCode(), @"image/Gif");
        }

                /// <summary>
        /// 获取用户登录信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        [HandlerLogin(FilterMode.Enforce)]
        public ActionResult GetUserInfo()
        {
            var data = LoginUserInfo.Get();
            data.imUrl = Config.GetValue("IMUrl");
            data.imOpen = Config.GetValue("IMOpen");
            data.password = null;
            data.secretkey = null;
            return Success(data);
        }

        #endregion

    }
}