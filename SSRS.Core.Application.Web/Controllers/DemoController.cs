﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SSRS.Core.Application.Web;
using SSRS.Core.Application.Web.Controllers;
using SSRS.PlugIns.Base.AuthorizeModule;
using SSRS.PlugIns.Base.SystemModule;
using SSRS.PlugIns.Organization;
using SSRS.Util;
using SSRS.Util.Operat;

namespace SSRS.Application.Web.Controllers
{
    [HandlerLogin(FilterMode.Ignore)]
    public class DemoController : BaseController
    {

        public IActionResult Index()
        {

            return View();
        }
    }
}