﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Autofac.Extras.DynamicProxy;
using log4net;
using log4net.Config;
using log4net.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using SSRS.Autofac;
using SSRS.Util;
using SSRS.Util.Codes;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using HttpContext = Microsoft.AspNetCore.Http.HttpContext;

namespace SSRS.Core.Application.Web
{
    public class Startup
    {

        #region Autofac注册
        public IContainer ApplicationContainer { get; private set; }
        public static ILoggerRepository LoggerRepository { get; set; }

        

        #endregion
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;


            #region 添加log4net 配置支持

            LoggerRepository = LogManager.CreateRepository("NETCoreRepository");
            //指定配置文件
            dynamic type = (new Program()).GetType();
            string currentDirectory = Path.GetDirectoryName(type.Assembly.Location);
            var fileInfo = new FileInfo(Path.Combine(currentDirectory, "XmlConfig\\log4net.config"));
            XmlConfigurator.Configure(LoggerRepository, fileInfo);
            #endregion
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            
            
            services.Replace(ServiceDescriptor.Transient<IControllerActivator, ServiceBasedControllerActivator>());
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

           
            services.AddSession(); 
            services.MyAddHttpContextAccessor();
            this.ApplicationContainer = SrAutofacCore.RegisterAutofac(services);
            SrAutoContainer.AutoContainer = this.ApplicationContainer;
            
            return new AutofacServiceProvider(this.ApplicationContainer);
             
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IConfiguration configuration, IApplicationLifetime applicationLifetime, IServiceProvider svp)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSession();
            
            app.UseStaticHttpContext();
            app.UseHttpsRedirection();
            app.UseStaticFiles();


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "areas",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
