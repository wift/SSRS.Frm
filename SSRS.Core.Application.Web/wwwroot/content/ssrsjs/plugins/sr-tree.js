﻿/*
 *创建人：徐志强
 *日期：2019年7月22日
 *描述：树形菜单的封装
 */

(function ($, srAdmin) {


    $.fn.lsrtree = function (settings) {


        var property = {
            elem: "",//指向容器选择器
            data: [],// Array
            id: "",//设定实例唯一索引，用于基础方法传参使用。
            showCheckbox: false,//是否显示复选框
            edit: false,//是否开启节点的操作图标。默认 false。 若为 true，则默认显示“改删”图标 若为 数组，则可自由配置操作图标的显示状态和顺序，目前支持的操作图标有：add、update、del，如：   edit: ['add', 'update', 'del']
            accordion: false,//是否开启手风琴模式，默认 false
            onlyIconControl: false,//是否仅允许节点左侧图标控制展开收缩。默认 false（即点击节点本身也可控制）。若为 true，则只能通过节点左侧图标来展开收缩
            isJump: false,//是否允许点击节点时弹出新窗口跳转。默认 false，若开启，需在节点数据中设定 link 参数（值为 url 格式）
            showLine: true, //是否开启连接线。默认 true，若设为 false，则节点左侧出现三角图标。
            text: {}//自定义各类默认文本，目前支持以下设定：
            /*
             * text: {
                     defaultNodeName: '未命名' //节点默认名称
                    ,none: '无数据' //数据为空时的提示文本
                }
             */
            , url: ""//Ajax 使用参数
            , propertyText: "",//树形菜单显示节点内容
            propertyValue: "",//树形菜单节点的值,
            method: "GET",//Ajax默认获取数据的方式为Ajax,
            param: {} //post提交的参数。
        }
        var $self = $(this);
        $.extend(property, settings);

        if (!!property.url || property.url!="") {

            var resData = srAdmin.httpGet(property.url, property.param);
            if (resData.code==200) {
                //有数据
                property.data = resData.data;
            }
        }
        //测试数据

       


        if (property.id == null || property.id == "") {
            property.id = "srtree" + new Date().getTime();
        }
        var tree = layui.tree;


        tree.render(property);



       

    }


})(jQuery, top.srAdmin);