﻿////////////////////////////////////////////////////////////
/*
 * @Describe: div大小监听事件
 */
; (function ($, h, c) {
    var aArray = $([]),
        ereSize = $.resize = $.extend($.resize, {}),
        i,
        kSetTimeout = "setTimeout",
        jResize = "resize",
        dspEvent = jResize + "-special-event",
        bDelay = "delay",
        fThrottleWindow = "throttleWindow";
    ereSize[bDelay] = 250;
    ereSize[fThrottleWindow] = true;
    $.event.special[jResize] = {
        setup: function () {
            if (!ereSize[fThrottleWindow] && this[kSetTimeout]) {
                return false;
            }
            var lThis = $(this);
            aArray = aArray.add(lThis);
            $.data(this, dspEvent, {
                w: lThis.width(),
                h: lThis.height()
            });
            if (aArray.length === 1) {
                g();
            }
        },
        teardown: function () {
            if (!ereSize[fThrottleWindow] && this[kSetTimeout]) {
                return false;
            }
            var l = $(this);
            aArray = aArray.not(l);
            l.removeData(dspEvent);
            if (!aArray.length) {
                clearTimeout(i);
            }
        },
        add: function (l) {
            if (!ereSize[fThrottleWindow] && this[kSetTimeout]) {
                return false;
            }
            var n;
            function m(s, o, p) {
                var q = $(this),
                    r = $.data(this, dspEvent);
                r.w = o !== c ? o : q.width();
                r.h = p !== c ? p : q.height();
                n.apply(this, arguments);
            }
            if ($.isFunction(l)) {
                n = l;
                return m;
            } else {
                n = l.handler;
                l.handler = m;
            }
        }
    };
    function g() {
        i = h[kSetTimeout](function () {
            aArray.each(function () {
                var n = $(this),
                    m = n.width(),
                    l = n.height(),
                    o = $.data(this, dspEvent);
                if (m !== o.w || l !== o.h) {
                    n.trigger(jResize, [o.w = m, o.h = l]);
                }
            });
            g();
        },
            ereSize[bDelay]);
    }
})(jQuery, this);



////////////////////////////////////////////////////////////


/*
 * @Describe: 鼠标滚动监听事件
 */
;(function ($) {
    var toFix = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'],
        toBind = ('onwheel' in document || document.documentMode >= 9) ?
            ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'],
        slice = Array.prototype.slice,
        nullLowestDeltaTimeout, lowestDelta;

    if ($.event.fixHooks) {
        for (var i = toFix.length; i;) {
            $.event.fixHooks[toFix[--i]] = $.event.mouseHooks;
        }
    }

    var special = $.event.special.mousewheel = {
        setup: function () {
            if (this.addEventListener) {
                for (var i = toBind.length; i;) {
                    this.addEventListener(toBind[--i], handler, false);
                }
            } else {
                this.onmousewheel = handler;
            }
            // Store the line height and page height for this particular element
            $.data(this, 'mousewheel-line-height', special.getLineHeight(this));
            $.data(this, 'mousewheel-page-height', special.getPageHeight(this));
        },
        teardown: function () {
            if (this.removeEventListener) {
                for (var i = toBind.length; i;) {
                    this.removeEventListener(toBind[--i], handler, false);
                }
            } else {
                this.onmousewheel = null;
            }
        },
        getLineHeight: function (elem) {
            return parseInt($(elem)['offsetParent' in $.fn ? 'offsetParent' : 'parent']().css('fontSize'), 10);
        },
        getPageHeight: function (elem) {
            return $(elem).height();
        },
        settings: {
            adjustOldDeltas: true
        }
    };

    $.fn.extend({
        mousewheel: function (fn) {
            return fn ? this.bind('mousewheel', fn) : this.trigger('mousewheel');
        },
        unmousewheel: function (fn) {
            return this.unbind('mousewheel', fn);
        }
    });


    function handler(event) {
        var orgEvent = event || window.event,
            args = slice.call(arguments, 1),
            delta = 0,
            deltaX = 0,
            deltaY = 0,
            absDelta = 0;
        event = $.event.fix(orgEvent);
        event.type = 'mousewheel';

        // Old school scrollwheel delta
        if ('detail' in orgEvent) { deltaY = orgEvent.detail * -1; }
        if ('wheelDelta' in orgEvent) { deltaY = orgEvent.wheelDelta; }
        if ('wheelDeltaY' in orgEvent) { deltaY = orgEvent.wheelDeltaY; }
        if ('wheelDeltaX' in orgEvent) { deltaX = orgEvent.wheelDeltaX * -1; }

        // Firefox < 17 horizontal scrolling related to DOMMouseScroll event
        if ('axis' in orgEvent && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
            deltaX = deltaY * -1;
            deltaY = 0;
        }

        // Set delta to be deltaY or deltaX if deltaY is 0 for backwards compatabilitiy
        delta = deltaY === 0 ? deltaX : deltaY;

        // New school wheel delta (wheel event)
        if ('deltaY' in orgEvent) {
            deltaY = orgEvent.deltaY * -1;
            delta = deltaY;
        }
        if ('deltaX' in orgEvent) {
            deltaX = orgEvent.deltaX;
            if (deltaY === 0) { delta = deltaX * -1; }
        }

        // No change actually happened, no reason to go any further
        if (deltaY === 0 && deltaX === 0) { return; }

        // Need to convert lines and pages to pixels if we aren't already in pixels
        // There are three delta modes:
        //   * deltaMode 0 is by pixels, nothing to do
        //   * deltaMode 1 is by lines
        //   * deltaMode 2 is by pages
        if (orgEvent.deltaMode === 1) {
            var lineHeight = $.data(this, 'mousewheel-line-height') || 10;
            delta *= lineHeight;
            deltaY *= lineHeight;
            deltaX *= lineHeight;
        } else if (orgEvent.deltaMode === 2) {
            var pageHeight = $.data(this, 'mousewheel-page-height') || 10;
            delta *= pageHeight;
            deltaY *= pageHeight;
            deltaX *= pageHeight;
        }

        // Store lowest absolute delta to normalize the delta values
        absDelta = Math.max(Math.abs(deltaY), Math.abs(deltaX));

        if (!lowestDelta || absDelta < lowestDelta) {
            lowestDelta = absDelta;

            // Adjust older deltas if necessary
            if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
                lowestDelta /= 40;
            }
        }

        // Adjust older deltas if necessary
        if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
            // Divide all the things by 40!
            delta /= 40;
            deltaX /= 40;
            deltaY /= 40;
        }

        // Get a whole, normalized value for the deltas
        delta = Math[delta >= 1 ? 'floor' : 'ceil'](delta / lowestDelta);
        deltaX = Math[deltaX >= 1 ? 'floor' : 'ceil'](deltaX / lowestDelta);
        deltaY = Math[deltaY >= 1 ? 'floor' : 'ceil'](deltaY / lowestDelta);

        // Add information to the event object
        event.deltaX = deltaX;
        event.deltaY = deltaY;
        event.deltaFactor = lowestDelta;
        // Go ahead and set deltaMode to 0 since we converted to pixels
        // Although this is a little odd since we overwrite the deltaX/Y
        // properties with normalized deltas.
        event.deltaMode = 0;

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        // Clearout lowestDelta after sometime to better
        // handle multiple device types that give different
        // a different lowestDelta
        // Ex: trackpad = 3 and mouse wheel = 120
        if (nullLowestDeltaTimeout) { clearTimeout(nullLowestDeltaTimeout); }
        nullLowestDeltaTimeout = setTimeout(nullLowestDelta, 200);

        return ($.event.dispatch || $.event.handle).apply(this, args);
    }
    function nullLowestDelta() {
        lowestDelta = null;
    }
    function shouldAdjustOldDeltas(orgEvent, absDelta) {
        return special.settings.adjustOldDeltas && orgEvent.type === 'mousewheel' && absDelta % 120 === 0;
    }
})(window.jQuery);


/*
 * @Describe: 基础框架
 */
/////////////////////////////////////////////////////////////
;top.srAdmin = (function ($) {
    "use strict";
    var srAdmin = {
        // 是否是调试模式
        isDebug: true,
        log: function () {
            if (srAdmin.isDebug) {
                console.log('--------------SSRS Adms JsA 1.0.0 -----------');
                var len = arguments.length;
                for (var i = 0; i < len; i++) {
                    console.log(arguments[i]);
                }
                console.log('--------------------------------------------');
            }
        },
        // 创建一个GUID
        newGuid: function () {
            var guid = "";
            for (var i = 1; i <= 32; i++) {
                var n = Math.floor(Math.random() * 16.0).toString(16);
                guid += n;
                if ((i == 8) || (i == 12) || (i == 16) || (i == 20)) guid += "-";
            }
            return guid;
        },
        // 加载提示
        loading: function (isShow, _text) {//加载动画显示与否
            var $loading = top.$("#sr_loading_bar");
            if (!!_text) {
                top.srAdmin.language.get(_text, function (text) {
                    top.$("#sr_loading_bar_message").html(text);
                });

            } else {
                top.srAdmin.language.get("正在拼了命为您加载…", function (text) {
                    top.$("#sr_loading_bar_message").html(text);
                });
            }
            if (isShow) {
                $loading.show();
            } else {
                $loading.hide();
            }
        },
        // 动态加载css文件
        loadStyles: function (url) {
            var link = document.createElement("link");
            link.type = "text/css";
            link.rel = "stylesheet";
            link.back = "backhr";
            link.href = url;
            document.getElementsByTagName("head")[0].appendChild(link);
        },
        // 获取iframe窗口
        iframe: function (Id, _frames) {
            if (_frames[Id] != undefined) {
                if (_frames[Id].contentWindow != undefined) {
                    return _frames[Id].contentWindow;
                }
                else {
                    return _frames[Id];
                }
            }
            else {
                return null;
            }
        },
        // 改变url参数值
        changeUrlParam: function (url, key, value) {
            var newUrl = "";
            var reg = new RegExp("(^|)" + key + "=([^&]*)(|$)");
            var tmp = key + "=" + value;
            if (url.match(reg) != null) {
                newUrl = url.replace(eval(reg), tmp);
            } else {
                if (url.match("[\?]")) {
                    newUrl = url + "&" + tmp;
                }
                else {
                    newUrl = url + "?" + tmp;
                }
            }
            return newUrl;
        },
        // 转化成十进制
        toDecimal: function (num) {
            if (num == null) {
                num = "0";
            }
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            var sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            var cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + '' +
                    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        },
        // 文件大小转换
        countFileSize: function (size) {
            if (size < 1024.00)
                return srAdmin.toDecimal(size) + " 字节";
            else if (size >= 1024.00 && size < 1048576)
                return srAdmin.toDecimal(size / 1024.00) + " KB";
            else if (size >= 1048576 && size < 1073741824)
                return srAdmin.toDecimal(size / 1024.00 / 1024.00) + " MB";
            else if (size >= 1073741824)
                return srAdmin.toDecimal(size / 1024.00 / 1024.00 / 1024.00) + " GB";
        },
        // 数组复制
        arrayCopy: function (data) {
            return $.map(data, function (obj) {
                return $.extend(true, {}, obj);
            });
        },
        // 检测数据是否选中
        checkrow: function (id) {
            var isOK = true;
            if (id == undefined || id == "" || id == 'null' || id == 'undefined') {
                isOK = false;
                top.srAdmin.language.get('您没有选中任何数据项,请选中后再操作！', function (text) {
                    srAdmin.alert.warning(text);
                });

            }
            return isOK;
        },
        // 提示消息栏
        alert: {
            success: function (msg) {
                top.srAdmin.language.get(msg, function (text) {
                    toastr.success(text);
                });

            },
            info: function (msg) {
                top.srAdmin.language.get(msg, function (text) {
                    toastr.info(text);
                });
            },
            warning: function (msg) {
                top.srAdmin.language.get(msg, function (text) {
                    toastr.warning(text);
                });
            },
            error: function (msg) {
                top.srAdmin.language.get(msg, function (text) {
                    toastr.error(msg);
                });
            }
        },
        //下载文件（she写的扩展）
        download: function (options) {
            var defaults = {
                method: "GET",
                url: "",
                param: []
            };
            var options = $.extend(defaults, options);
            if (options.url && options.param) {
                var $form = $('<form action="' + options.url + '" method="' + (options.method || 'post') + '"></form>');
                for (var key in options.param) {
                    var $input = $('<input type="hidden" data-back="backhr"/>').attr('name', key).val(options.param[key]);
                    $form.append($input);
                }
                $form.appendTo('body').submit().remove();
            };
        },

        // 数字格式转换成千分位
        commafy: function (num) {
            if (num == null) {
                num = "0";
            }
            if (isNaN(num)) {
                return "0";
            }
            num = num + "";
            if (/^.*\..*$/.test(num)) {
                varpointIndex = num.lastIndexOf(".");
                varintPart = num.substring(0, pointIndex);
                varpointPart = num.substring(pointIndex + 1, num.length);
                intPart = intPart + "";
                var re = /(-?\d+)(\d{3})/
                while (re.test(intPart)) {
                    intPart = intPart.replace(re, "$1,$2");
                }
                num = intPart + "." + pointPart;
            } else {
                num = num + "";
                var re = /(-?\d+)(\d{3})/
                while (re.test(num)) {
                    num = num.replace(re, "$1,$2");
                }
            }
            return num;
        },

        // 检测图片是否存在
        isExistImg: function (pathImg) {
            if (!!pathImg) {
                var ImgObj = new Image();
                ImgObj.src = pathImg;
                if (ImgObj.fileSize > 0 || (ImgObj.width > 0 && ImgObj.height > 0)) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
    };
    return srAdmin;
})(window.jQuery);



////////////////////////////////////////////////////////////

/*
 * @Describe:优化滚动条
 */
;(function ($, srAdmin, window) {
    "use strict";
    var $move = null;

    var methods = {
        init: function ($this, callback) {
            var id = $this.attr('id');
            if (!id) {
                id = 'sr_' + srAdmin.newGuid();
                $this.attr('id', id);
            }

            $this.addClass('sr-scroll-wrap');
            // 加载内容
            var $content = $this.children();

            var $scroll = $('<div class="sr-scroll-box" id="' + id + '_box" ></div>');
            $this.append($scroll);
            $scroll.append($content);

            // 加载y滚动条
            var $vertical = $('<div class="sr-scroll-vertical"   ><div class="sr-scroll-vertical-block" id="' +
                id +
                '_vertical"></div></div>');
            $this.append($vertical);

            // 加载x滚动条
            var $horizontal = $('<div class="sr-scroll-horizontal" ><div class="sr-scroll-horizontal-block" id="' +
                id +
                '_horizontal"></div></div>');
            $this.append($horizontal);

            // 添加一个移动板
            if ($move === null) {
                $move = $('<div style="-moz-user-select: none;-webkit-user-select: none;-ms-user-select: none;-khtml-user-select: none;user-select: none;display: none;position: fixed;top: 0;left: 0;width: 100%;height: 100%;z-index: 9999;cursor: pointer;" ></div>');
                $('body').append($move);
            }
            // 初始化数据
            var sh = $scroll.innerHeight();
            var sw = $scroll.innerWidth();


            var h = $this.height();
            var w = $this.width();
            var data = {
                id: id,
                sy: 0,
                sx: 0,
                sh: sh,
                sw: sw,
                h: h,
                w: w,
                yh: 0,
                xw: 0,
                callback: callback
            };
            $this[0].op = data;
            methods.update($this);
            methods.bindEvent($this, $scroll, $vertical, $horizontal);

            $scroll = null;
            $content = null;
            $vertical = null;
            $horizontal = null;
            $this = null;
        },
        bindEvent: function ($this, $scroll, $vertical, $horizontal) { // 绑定监听事件
            // div大小变化
            $this.resize(function () {
                var $this = $(this);
                var op = $this[0].op;
                if (op) {
                    var h = $this.height();
                    var w = $this.width();
                    if (h != op.h) {
                        op.h = h;
                        methods.updateY($this);
                    }
                    if (w != op.w) {
                        op.w = w;
                        methods.updateX($this);
                    }
                }
                $this = null;
            });
            $scroll.resize(function () {
                var $this = $(this);
                var $scrollWrap = $this.parent();
                var op = $scrollWrap[0].op;
                if (op) {
                    var sh = $this.innerHeight();
                    var sw = $this.innerWidth();

                    if (sh != op.sh) {
                        op.sh = sh;
                        methods.updateY($scrollWrap);
                    }
                    if (sw != op.sw) {
                        op.sw = sw;
                        methods.updateX($scrollWrap);
                    }
                }
                $this = null;
                $scrollWrap = null;
            });

            // 监听鼠标滚动
            $this.mousewheel(function (event, delta, deltaX, deltaY) {
                var $this = $(this);
                var $select = $this.find('.sr-select-focus');
                if ($select.length > 0) {
                    var selectId = "srAdmin_select_option_" + $select.attr('id');
                    $('#' + selectId).slideUp(150);
                    $select.removeClass('sr-select-focus');
                }

                var _v = 4 + (Math.abs(delta) - 1) * 0.1 * event.deltaFactor;
                if (_v > 16 && _v < 300) {
                    _v = 16;
                }
                else if (_v >= 300) {
                    _v = 20;
                }
                var op = $this[0].op;
                var d = delta * _v;
                if (op.sh > op.h) {
                    op.oldsy = op.sy;
                    op.sy = op.sy - d;
                    setTimeout(function () {
                        methods.moveY($this, true, true);
                        $this = null;
                    });
                    if (op.h + op.sy < op.sh && op.sy > 0) {
                        return false;
                    }
                } else if (op.sw > op.w) {
                    op.oldsx = op.sx;
                    op.sx = op.sx - d;
                    setTimeout(function () {
                        methods.moveX($this, true);
                        $this = null;
                    });
                    return false;
                }
            });

            // 监听鼠标移动
            $vertical.find('.sr-scroll-vertical-block').on('mousedown', function (e) {
                $move.show();
                var $this = $(this).parent().parent();
                var op = $this[0].op;
                op.isYMousedown = true;
                op.yMousedown = e.pageY;
                $this.addClass('sr-scroll-active');
                $this = null;
            });
            $horizontal.find('.sr-scroll-horizontal-block').on('mousedown', function (e) {
                $move.show();
                var $this = $(this).parent().parent();
                var op = $this[0].op;
                op.isXMousedown = true;
                op.xMousedown = e.pageX;
                $this.addClass('sr-scroll-active');
                $this = null;
            });


            top.$(document).on('mousemove', { $obj: $this }, function (e) {
                var op = e.data.$obj[0].op;

                if (op.isYMousedown) {

                    var $select = e.data.$obj.find('.sr-select-focus');
                    if ($select.length > 0) {
                        var selectId = "srAdmin_select_option_" + $select.attr('id');
                        $('#' + selectId).slideUp(150);
                        $select.removeClass('sr-select-focus');
                    }

                    var y = e.pageY;
                    var _yd = y - op.yMousedown;
                    op.yMousedown = y;
                    op.oldsy = op.sy;
                    op.blockY = op.blockY + _yd;

                    if ((op.blockY + op.yh) > op.h) {
                        op.blockY = op.h - op.yh;
                    }
                    if (op.blockY < 0) {
                        op.blockY = 0;
                    }
                    methods.getY(op);
                    methods.moveY(e.data.$obj, true);
                }
                else if (op.isXMousedown) {
                    var $select = e.data.$obj.find('.sr-select-focus');
                    if ($select.length > 0) {
                        var selectId = "srAdmin_select_option_" + $select.attr('id');
                        $('#' + selectId).slideUp(150);
                        $select.removeClass('sr-select-focus');
                    }

                    var x = e.pageX;
                    var _xd = x - op.xMousedown;
                    op.xMousedown = x;
                    op.oldsx = op.sx;
                    op.blockX = op.blockX + _xd;
                    if ((op.blockX + op.xw) > op.w) {
                        op.blockX = op.w - op.xw;
                    }
                    if (op.blockX < 0) {
                        op.blockX = 0;
                    }
                    methods.getX(op);
                    methods.moveX(e.data.$obj);
                }
            }).on('mouseup', { $obj: $this }, function (e) {
                e.data.$obj[0].op.isYMousedown = false;
                e.data.$obj[0].op.isXMousedown = false;
                $move.hide();
                e.data.$obj.removeClass('sr-scroll-active');
            });
        },
        update: function ($this) { // 更新滚动条
            methods.updateY($this);
            methods.updateX($this);
        },
        updateY: function ($this) {
            var op = $this[0].op;
            var $scroll = $this.find('#' + op.id + '_box');
            var $vertical = $this.find('#' + op.id + '_vertical');
            if (op.sh > op.h) { // 出现纵向滚动条
                // 更新显示区域位置
                if ((op.sh - op.sy) < op.h) {
                    var _sy = 0;
                    op.sy = op.sh - op.h;
                    if (op.sy < 0) {
                        op.sy = 0;
                    } else {
                        _sy = 0 - op.sy;
                    }
                    $scroll.css('top', _sy + 'px');
                }
                // 更新滚动条高度
                var scrollH = parseInt(op.h * op.h / op.sh);
                scrollH = (scrollH < 30 ? 30 : scrollH);
                op.yh = scrollH;

                // 更新滚动条位置
                var _y = parseInt(op.sy * (op.h - scrollH) / (op.sh - op.h));
                if ((_y + scrollH) > op.h) {
                    _y = op.h - scrollH;
                }
                if (_y < 0) {
                    _y = 0;
                }

                op.blockY = _y;

                // 设置滚动块大小和位置
                $vertical.css({
                    'top': _y + 'px',
                    'height': scrollH + 'px'
                });
            } else {
                op.blockY = 0;
                op.sy = 0;
                $scroll.css('top', '0px');
                $vertical.css({
                    'top': '0px',
                    'height': '0px'
                });
            }

            op.callback && op.callback(op.sx, op.sy);
            $scroll = null;
            $vertical = null;
        },
        updateX: function ($this) {
            var op = $this[0].op;
            var $scroll = $this.find('#' + op.id + '_box');
            var $horizontal = $this.find('#' + op.id + '_horizontal');
            if (op.sw > op.w) {
                // 更新显示区域位置
                if ((op.sw - op.sx) < op.w) {
                    var _sx = 0;
                    op.sx = op.sw - op.w;
                    if (op.sx < 0) {
                        op.sx = 0;
                    } else {
                        _sx = 0 - op.sx;
                    }
                    $scroll.css('left', _sx + 'px');
                }
                // 更新滚动条高度
                var scrollW = parseInt(op.w * op.w / op.sw);
                scrollW = (scrollW < 30 ? 30 : scrollW);
                op.xw = scrollW;

                // 更新滚动条位置
                var _x = parseInt(op.sx * (op.w - scrollW) / (op.sw - op.w));
                if ((_x + scrollW) > op.w) {
                    _x = op.w - scrollW;
                }
                if (_x < 0) {
                    _x = 0;
                }
                op.blockX = _x;
                // 设置滚动块大小和位置
                $horizontal.css({
                    'left': _x + 'px',
                    'width': scrollW + 'px'
                });

            } else {
                op.sx = 0;
                op.blockX = 0;
                $scroll.css('left', '0px');
                $horizontal.css({
                    'left': '0px',
                    'width': '0px'
                });
            }
            op.callback && op.callback(op.sx, op.sy);
            $scroll = null;
            $horizontal = null;
        },
        moveY: function ($this, isMousewheel, isCallBack) {
            var op = $this[0].op;
            var $scroll = $this.find('#' + op.id + '_box');
            var $vertical = $this.find('#' + op.id + '_vertical');

            // 更新显示区域位置
            var _sy = 0;
            if (op.sy < 0) {
                op.sy = 0;
            } else if (op.sy + op.h > op.sh) {
                op.sy = op.sh - op.h;
                _sy = 0 - op.sy;
            } else {
                _sy = 0 - op.sy;
            }
            if (isMousewheel) {
                var _y = methods.getBlockY(op);
                if (_y == 0 && op.sy != 0) {
                    op.sy = 0;
                    _sy = 0;
                }
                op.blockY = _y;
                // 设置滚动块位置
                //var d = Math.abs(op.sy - op.oldsy) * 100 / 4;
                $scroll.css({
                    'top': _sy + 'px'
                });
                $vertical.css({
                    'top': _y + 'px'
                });
            } else {
                $scroll.css({
                    'top': _sy + 'px'
                });
                $vertical.css({
                    'top': op.blockY + 'px'
                });
            }
            if (isCallBack) {
                op.callback && op.callback(op.sx, op.sy);
            }
            $scroll = null;
            $vertical = null;
        },
        moveX: function ($this, isMousewheel) {
            var op = $this[0].op;
            var $scroll = $this.find('#' + op.id + '_box');
            var $horizontal = $this.find('#' + op.id + '_horizontal');

            // 更新显示区域位置
            var _sx = 0;
            if (op.sx < 0) {
                op.sx = 0;
            } else if (op.sx + op.w > op.sw) {
                op.sx = op.sw - op.w;
                _sx = 0 - op.sx;
            } else {
                _sx = 0 - op.sx;
            }

            if (isMousewheel) {
                // 更新滑块的位置
                var _x = methods.getBlockX(op);
                if (_x == 0 && op.sx != 0) {
                    op.sx = 0;
                    _sx = 0;
                }
                op.blockX = _x;
                // 设置滚动块位置
                //var d = Math.abs(op.sx - op.oldsx) * 100 / 4;
                $scroll.css({
                    'left': _sx + 'px'
                });
                $horizontal.css({
                    'left': _x + 'px'
                });
            } else {
                $scroll.css({
                    'left': _sx + 'px'
                });
                $horizontal.css({
                    'left': op.blockX + 'px'
                });
            }
            op.callback && op.callback(op.sx, op.sy);
            $scroll = null;
            $horizontal = null;

        },
        getBlockY: function (op) {
            var _y = parseFloat(op.sy * (op.h - op.yh) / (op.sh - op.h));
            if ((_y + op.yh) > op.h) {
                _y = op.h - op.yh;
            }
            if (_y < 0) {
                _y = 0;
            }
            return _y;
        },
        getY: function (op) {
            op.sy = parseInt(op.blockY * (op.sh - op.h) / (op.h - op.yh));
            if ((op.sy + op.h) > op.sh) {
                op.sy = op.sh - op.h;
            }
            if (op.sy < 0) {
                op.sy = 0;
            }
        },
        getBlockX: function (op) {
            var _x = parseFloat(op.sx * (op.w - op.xw) / (op.sw - op.w));
            if ((_x + op.xw) > op.w) {
                _x = op.w - op.xw;
            }
            if (_x < 0) {
                _x = 0;
            }
            return _x;
        },
        getX: function (op) {
            op.sx = parseInt(op.blockX * (op.sw - op.w) / (op.w - op.xw));
            if ((op.sx + op.w) > op.sw) {
                op.sx = op.sw - op.w;
            }
            if (op.sx < 0) {
                op.sx = 0;
            }
        },
    };
    $.fn.srscroll = function (callback) {
        $(this).each(function () {
            var $this = $(this);
            methods.init($this, callback);
        });
    }

    $.fn.srscrollSet = function (name, data, isCallBack) {
        var $this = $(this);
        if (!$this[0] || !$this[0].op) {
            return;
        }
        switch (name) {
            case 'moveRight':
                setTimeout(function () {
                    var op = $this[0].op;
                    op.oldsx = op.sx;
                    op.sx = op.sw - op.w;
                    methods.moveX($this, true);
                    $this = null;
                }, 250);
                break;
            case 'moveBottom':
                setTimeout(function () {
                    var op = $this[0].op;
                    op.oldsy = op.sy;
                    op.sy = op.sh - op.h;
                    methods.moveY($this, true, isCallBack);
                    $this = null;
                }, 250);
                break;
            case 'moveY':
                setTimeout(function () {
                    var op = $this[0].op;
                    op.oldsy = op.sy;
                    op.sy = data;
                    methods.moveY($this, true, isCallBack);
                    $this = null;
                });
                break;
            case 'moveX':
                break;
        }
    }

})(window.jQuery, top.srAdmin, window);



////////////////////////////////////////////////////////////
/*
 * @Describe：时间日期的处理
 */
;(function ($, srAdmin) {
    "use strict";
    $.extend(srAdmin, {
        // 字串转化成日期
        parseDate: function (strDate) {
            var myDate;
            if (strDate.indexOf("/Date(") > -1)
                myDate = new Date(parseInt(strDate.replace("/Date(", "").replace(")/", ""), 10));
            else
                myDate = new Date(Date.parse(strDate.replace(/-/g, "/").replace("T", " ").split(".")[0]));//.split(".")[0] 用来处理出现毫秒的情况，截取掉.xxx，否则会出错
            return myDate;
        },
        // 日期格式化v日期,format:格式
        formatDate: function (v, format) {
            if (!v) return "";
            var d = v;
            if (typeof v === 'string') {
                if (v.indexOf("/Date(") > -1)
                    d = new Date(parseInt(v.replace("/Date(", "").replace(")/", ""), 10));
                else
                    d = new Date(Date.parse(v.replace(/-/g, "/").replace("T", " ").split(".")[0]));//.split(".")[0] 用来处理出现毫秒的情况，截取掉.xxx，否则会出错
            }
            var o = {
                "M+": d.getMonth() + 1,  //month
                "d+": d.getDate(),       //day
                "h+": d.getHours(),      //hour
                "m+": d.getMinutes(),    //minute
                "s+": d.getSeconds(),    //second
                "q+": Math.floor((d.getMonth() + 3) / 3),  //quarter
                "S": d.getMilliseconds() //millisecond
            };
            if (/(y+)/.test(format)) {
                format = format.replace(RegExp.$1, (d.getFullYear() + "").substr(4 - RegExp.$1.length));
            }
            for (var k in o) {
                if (new RegExp("(" + k + ")").test(format)) {
                    format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
                }
            }
            return format;
        },
        // 获取当前时间;format为格式
        getDate: function (format, strInterval, Number) {
            var myDate = new Date();
            if (!!strInterval) {
                myDate = myDate.DateAdd(strInterval, Number);
            }
            var res = srAdmin.formatDate(myDate, format);
            return res;
        },
        // 月
        getMonth: function () {
            var res = {
                begin: '',
                end: ''
            };
            var currentDate = srAdmin.parseDate(srAdmin.formatDate(new Date(), "yyyy-MM-01"));
            var endDate = currentDate.DateAdd('m', 1).DateAdd('d', -1);

            res.begin = srAdmin.formatDate(currentDate, 'yyyy-MM-dd 00:00:00');
            res.end = srAdmin.formatDate(endDate, 'yyyy-MM-dd 23:59:59');

            return res;
        },
        getPreMonth: function () {
            var res = {
                begin: '',
                end: ''
            };
            var currentDate = srAdmin.parseDate(srAdmin.formatDate(new Date(), "yyyy-MM-01"));
            var preMonth = currentDate.DateAdd('d', -1);

            res.begin = srAdmin.formatDate(preMonth, 'yyyy-MM-01 00:00:00');
            res.end = srAdmin.formatDate(preMonth, 'yyyy-MM-dd 23:59:59');

            return res;
        },
        // 季度
        getCurrentQuarter: function () {
            var currentDate = new Date();
            return srAdmin.getQuarter(currentDate.getFullYear(), currentDate.getMonth());
        },
        getPreQuarter: function () {
            var currentDate = new Date().DateAdd('q', -1);
            return srAdmin.getQuarter(currentDate.getFullYear(), currentDate.getMonth());
        },
        getQuarter: function (Year, month) {
            var res = {
                begin: '',
                end: ''
            };
            switch (month) {
                case 0:
                case 1:
                case 2:
                    res.begin = Year + "-01-01 00:00:00";
                    res.end = Year + "-03-31 23:59:59";
                    break;
                case 3:
                case 4:
                case 5:
                    res.begin = Year + "-04-01 00:00:00";
                    res.end = Year + "-06-30 23:59:59";
                    break;
                case 6:
                case 7:
                case 8:
                    res.begin = Year + "-07-01 00:00:00";
                    res.end = Year + "-09-30 23:59:59";
                    break;
                case 9:
                case 10:
                case 11:
                    res.begin = Year + "-10-01 00:00:00";
                    res.end = Year + "-12-31 23:59:59";
                    break;
            }
            return res;
        },
        // 年
        getYear: function () {
            var currentDate = new Date();
            var res = {
                begin: '',
                end: ''
            };
            var year = currentDate.getFullYear();
            res.begin = year + '-01-01 00:00:00';
            res.end = year + '-12-31 23:59:59';
            return res;
        },
        getPreYear: function () {
            var currentDate = new Date();
            var res = {
                begin: '',
                end: ''
            };
            var year = currentDate.getFullYear() - 1;
            res.begin = year + '-01-01 00:00:00';
            res.end = year + '-12-31 23:59:59';
            return res;
        },
        getFirstHalfYear: function () {
            var currentDate = new Date();
            var res = {
                begin: '',
                end: ''
            };
            var year = currentDate.getFullYear();
            res.begin = year + '-01-01 00:00:00';
            res.end = year + '-06-30 23:59:59';
            return res;
        },
        getSecondHalfYear: function () {
            var currentDate = new Date();
            var res = {
                begin: '',
                end: ''
            };
            var year = currentDate.getFullYear();
            res.begin = year + '-07-01 00:00:00';
            res.end = year + '-12-31 23:59:59';
            return res;
        }
    });
    //+---------------------------------------------------  
    //| 日期计算  
    //+---------------------------------------------------  
    Date.prototype.DateAdd = function (strInterval, Number) {
        var dtTmp = this;
        switch (strInterval) {
            case 's': return new Date(Date.parse(dtTmp) + (1000 * Number));// 秒
            case 'n': return new Date(Date.parse(dtTmp) + (60000 * Number));// 分
            case 'h': return new Date(Date.parse(dtTmp) + (3600000 * Number));// 小时
            case 'd': return new Date(Date.parse(dtTmp) + (86400000 * Number));// 天
            case 'w': return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number));// 星期
            case 'q': return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number * 3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());// 季度
            case 'm': return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());// 月
            case 'y': return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());// 年
        }
    }
    //+---------------------------------------------------  
    //| 比较日期差 dtEnd 格式为日期型或者 有效日期格式字符串  
    //+---------------------------------------------------  
    Date.prototype.DateDiff = function (strInterval, dtEnd) {
        var dtStart = this;
        if (typeof dtEnd == 'string')//如果是字符串转换为日期型  
        {
            dtEnd = srAdmin.parseDate(dtEnd);
        }
        switch (strInterval) {
            case 's': return parseInt((dtEnd - dtStart) / 1000);
            case 'n': return parseInt((dtEnd - dtStart) / 60000);
            case 'h': return parseInt((dtEnd - dtStart) / 3600000);
            case 'd': return parseInt((dtEnd - dtStart) / 86400000);
            case 'w': return parseInt((dtEnd - dtStart) / (86400000 * 7));
            case 'm': return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - dtStart.getFullYear()) * 12) - (dtStart.getMonth() + 1);
            case 'y': return dtEnd.getFullYear() - dtStart.getFullYear();
        }
    }
    //+---------------------------------------------------  
    //| 取得当前日期所在月的最大天数  
    //+---------------------------------------------------  
    Date.prototype.MaxDayOfDate = function () {
        var myDate = this;
        var date1 = srAdmin.parseDate(srAdmin.formatDate(myDate, 'yyyy-MM-01 00:00:00'));
        var date2 = date1.DateAdd('m', 1);
        var result = date1.DateDiff('d', date2);
        return result;
    }
    //---------------------------------------------------  
    // 判断闰年  
    //---------------------------------------------------  
    Date.prototype.isLeapYear = function () {
        return (0 == this.getYear() % 4 && ((this.getYear() % 100 != 0) || (this.getYear() % 400 == 0)));
    }
})(jQuery, top.srAdmin);



////////////////////////////////////////////////////////////

/*
 *  @Describe：数据验证完整性
 */
;(function ($, srAdmin) {
    "use strict";

    // 数据验证方法
    srAdmin.validator = {
        validReg: function (obj, reg, msg) {
            var res = { code: true, msg: '' };
            if (!reg.test(obj)) {
                res.code = false;
                res.msg = msg;
            }
            return res;
        },
        validRegOrNull: function (obj, reg, msg) {
            var res = { code: true, msg: '' };
            if (obj == null || obj == undefined || obj.length == 0) {
                return res;
            }
            if (!reg.test(obj)) {
                res.code = false;
                res.msg = msg;
            }
            return res;
        },
        isNotNull: function (obj) {// 验证不为空
            var res = { code: true, msg: '' };
            obj = $.trim(obj);
            if (obj == null || obj == undefined || obj.length == 0) {
                res.code = false;
                res.msg = '不能为空';
            }
            return res;
        },
        isNum: function (obj) {// 验证数字
            return srAdmin.validator.validReg(obj, /^[-+]?\d+$/, '必须为数字');
        },
        isNumOrNull: function (obj) {// 验证数字 或者空
            return srAdmin.validator.validRegOrNull(obj, /^[-+]?\d+$/, '数字或空');
        },
        isEmail: function (obj) {//Email验证 email
            return srAdmin.validator.validReg(obj, /^\w{3,}@\w+(\.\w+)+$/, '必须为E-mail格式');
        },
        isEmailOrNull: function (obj) {//Email验证 email   或者null,空
            return srAdmin.validator.validRegOrNull(obj, /^\w{3,}@\w+(\.\w+)+$/, '必须为E-mail格式或空');
        },
        isEnglishStr: function (obj) {//验证只能输入英文字符串 echar
            return srAdmin.validator.validReg(obj, /^[a-z,A-Z]+$/, '必须为英文字符串');
        },
        isEnglishStrOrNull: function (obj) {//验证只能输入英文字符串 echar 或者null,空
            return srAdmin.validator.validRegOrNull(obj, /^[a-z,A-Z]+$/, '必须为英文字符串或空');
        },
        isTelephone: function (obj) { //验证是否电话号码 phone
            return srAdmin.validator.validReg(obj, /^(\d{3,4}\-)?[1-9]\d{6,7}$/, '必须为电话格式');
        },
        isTelephoneOrNull: function (obj) {//验证是否电话号码 phone或者null,空
            return srAdmin.validator.validRegOrNull(obj, /^(\d{3,4}\-)?[1-9]\d{6,7}$/, '必须为电话格式或空');
        },
        isMobile: function (obj) {//验证是否手机号 mobile
            return srAdmin.validator.validReg(obj, /^(\+\d{2,3}\-)?\d{11}$/, '必须为手机格式');
        },
        isMobileOrnull: function (obj) {//验证是否手机号 mobile或者null,空
            return srAdmin.validator.validRegOrNull(obj, /^(\+\d{2,3}\-)?\d{11}$/, '必须为手机格式或空');
        },
        isMobileOrPhone: function (obj) {//验证是否手机号或电话号码 mobile phone 
            var res = { code: true, msg: '' };
            if (!srAdmin.validator.isTelephone(obj).code && !srAdmin.validator.isMobile(obj).code) {
                res.code = false;
                res.msg = '为电话格式或手机格式';
            }
            return res;
        },
        isMobileOrPhoneOrNull: function (obj) {//验证是否手机号或电话号码 mobile phone或者null,空
            var res = { code: true, msg: '' };
            if (srAdmin.validator.isNotNull(obj).code && !srAdmin.validator.isTelephone(obj).code && !srAdmin.validator.isMobile(obj).code) {
                res.code = false;
                res.msg = '为电话格式或手机格式或空';
            }
            return res;
        },
        isUri: function (obj) {//验证网址 uri
            return srAdmin.validator.validReg(obj, /^http:\/\/[a-zA-Z0-9]+\.[a-zA-Z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/, '必须为网址格式');
        },
        isUriOrNull: function (obj) {//验证网址 uri或者null,空
            return srAdmin.validator.validRegOrNull(obj, /^http:\/\/[a-zA-Z0-9]+\.[a-zA-Z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/, '必须为网址格式或空');
        },
        isDate: function (obj) {//判断日期类型是否为YYYY-MM-DD格式的类型 date
            return srAdmin.validator.validReg(obj, /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/, '必须为日期格式');
        },
        isDateOrNull: function (obj) {//判断日期类型是否为YYYY-MM-DD格式的类型 date或者null,空
            return srAdmin.validator.validRegOrNull(obj, /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/, '必须为日期格式或空');
        },
        isDateTime: function (obj) {//判断日期类型是否为YYYY-MM-DD hh:mm:ss格式的类型 datetime
            return srAdmin.validator.validReg(obj, /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/, '必须为日期时间格式');
        },
        isDateTimeOrNull: function (obj) {//判断日期类型是否为YYYY-MM-DD hh:mm:ss格式的类型 datetime或者null,空
            return srAdmin.validator.validRegOrNull(obj, /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/, '必须为日期时间格式');
        },
        isTime: function (obj) {//判断日期类型是否为hh:mm:ss格式的类型 time
            return srAdmin.validator.validReg(obj, /^((20|21|22|23|[0-1]\d)\:[0-5][0-9])(\:[0-5][0-9])?$/, '必须为时间格式');
        },
        isTimeOrNull: function (obj) {//判断日期类型是否为hh:mm:ss格式的类型 time或者null,空
            return srAdmin.validator.validRegOrNull(obj, /^((20|21|22|23|[0-1]\d)\:[0-5][0-9])(\:[0-5][0-9])?$/, '必须为时间格式或空');
        },
        isChinese: function (obj) {//判断输入的字符是否为中文 cchar 
            return srAdmin.validator.validReg(obj, /^[\u0391-\uFFE5]+$/, '必须为中文');
        },
        isChineseOrNull: function (obj) {//判断输入的字符是否为中文 cchar或者null,空
            return srAdmin.validator.validRegOrNull(obj, /^[\u0391-\uFFE5]+$/, '必须为中文或空');
        },
        isZip: function (obj) {//判断输入的邮编(只能为六位)是否正确 zip
            return srAdmin.validator.validReg(obj, /^\d{6}$/, '必须为邮编格式');
        },
        isZipOrNull: function (obj) {//判断输入的邮编(只能为六位)是否正确 zip或者null,空
            return srAdmin.validator.validRegOrNull(obj, /^\d{6}$/, '必须为邮编格式或空');
        },
        isDouble: function (obj) {//判断输入的字符是否为双精度 double
            return srAdmin.validator.validReg(obj, /^[-\+]?\d+(\.\d+)?$/, '必须为小数');
        },
        isDoubleOrNull: function (obj) {//判断输入的字符是否为双精度 double或者null,空
            return srAdmin.validator.validRegOrNull(obj, /^[-\+]?\d+(\.\d+)?$/, '必须为小数或空');
        },
        isIDCard: function (obj) {//判断是否为身份证 idcard
            return srAdmin.validator.validReg(obj, /^\d{15}(\d{2}[A-Za-z0-9;])?$/, '必须为身份证格式');
        },
        isIDCardOrNull: function (obj) {//判断是否为身份证 idcard或者null,空
            return srAdmin.validator.validRegOrNull(obj, /^\d{15}(\d{2}[A-Za-z0-9;])?$/, '必须为身份证格式或空');
        },
        isIP: function (obj) {//判断是否为IP地址格式
            var res = { code: true, msg: '' };
            var reg = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/g //匹配IP地址的正则表达式 
            var flag = false;
            if (reg.test(obj)) {
                if (RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256) { flag = true };
            }
            if (!flag) {
                res.code = false;
                res.msg = '必须为IP格式';
            }
            return res;
        },
        isIPOrNull: function (obj) {//判断是否为IP地址格式 或者null,空
            var res = { code: true, msg: '' };
            if (srAdmin.validator.isNotNull(obj) && !srAdmin.validator.isIP(obj).code) {
                res.code = false;
                res.msg = '必须为IP格式或空';
            }
            return res;
        },

        isLenNum: function (obj, n) {//验证是否是n位数字字符串编号 nnum
            var res = { code: true, msg: '' };
            var reg = /^[0-9]+$/;
            obj = $.trim(obj);
            if (obj.length > n || !reg.test(obj)) {
                res.code = false;
                res.msg = '必须为' + n + '位数字';
            }
            return res;
        },
        isLenNumOrNull: function (obj, n) {//验证是否是n位数字字符串编号 nnum或者null,空
            var res = { code: true, msg: '' };
            if (srAdmin.validator.isNotNull(obj).code && !srAdmin.validator.isLenNum(obj)) {
                res.code = false;
                res.msg = '必须为' + n + '位数字或空';
            }
            return res;
        },
        isLenStr: function (obj, n) {//验证是否小于等于n位数的字符串 nchar
            var res = { code: true, msg: '' };
            obj = $.trim(obj);
            if (!srAdmin.validator.isNotNull(obj).code || obj.length > n) {
                res.code = false;
                res.msg = '必须小于等于' + n + '位字符';
            }
            return res;
        },
        isLenStrOrNull: function (obj, n) {//验证是否小于等于n位数的字符串 nchar或者null,空
            var res = { code: true, msg: '' };
            obj = $.trim(obj);
            if (srAdmin.validator.isNotNull(obj).code && obj.length > n) {
                res.code = false;
                res.msg = '必须小于等于' + n + '位字符或空';
            }
            return res;
        }
    };

})(window.jQuery, top.srAdmin);

////////////////////////////////////////////////////////////


/*
 * @Describe：弹层（基于layer.js-3.0.3）
 */
;(function ($, srAdmin) {
    "use strict";
    $.extend(srAdmin, {
        // 询问框
        layerConfirm: function (_msg, callback) {
            top.srAdmin.language.get(_msg, function (msg) {
                top.layer.confirm(msg, {
                    btn: ['确认', '取消'],
                    title: "提示",
                    icon: 0,
                    skin: 'sr-layer',
                    success: function (layero, index) {
                        layero.find('.layui-layer-btn a').each(function () {
                            var $this = $(this);
                            var _text = $this.text();
                            top.srAdmin.language.get(_text, function (text) {
                                $this.text(text);
                            });

                        });
                        layero.find('.layui-layer-title').each(function () {
                            var $this = $(this);
                            var _text = $this.text();
                            top.srAdmin.language.get(_text, function (text) {
                                $this.text(text);
                            });

                        });
                    },
                }, function (index) {
                    callback(true, index);
                }, function (index) {
                    callback(false, index);
                    top.layer.close(index); //再执行关闭  
                });
            });


        },
        // 自定义表单弹层
        layerForm: function (op) {
            var dfop = {
                id: null,
                title: '系统窗口',
                width: 550,
                height: 400,
                url: 'error',
                btn: ['确认', '关闭'],
                callBack: false,
                maxmin: false,
                end: false,
            };
            $.extend(dfop, op || {});

            /*适应窗口大小*/
            dfop.width = dfop.width > $(window).width() ? $(window).width() - 10 : dfop.width;
            dfop.height = dfop.height > $(window).height() ? $(window).height() - 10 : dfop.height;

            var r = top.layer.open({
                id: dfop.id,
                maxmin: dfop.maxmin,
                type: 2,//0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
                title: dfop.title,
                area: [dfop.width + 'px', dfop.height + 'px'],
                btn: dfop.btn,
                content: op.url,
                skin: dfop.btn == null ? 'sr-layer-nobtn' : 'sr-layer',
                success: function (layero, index) {
                    top['layer_' + dfop.id] = srAdmin.iframe($(layero).find('iframe').attr('id'), top.frames);
                    layero[0].srAdmin_layerid = 'layer_' + dfop.id;
                    //如果底部有按钮添加-确认并关闭窗口勾选按钮
                    if (!!dfop.btn && layero.find('.sr-layer-btn-cb').length == 0) {
                        top.srAdmin.language.get('确认并关闭窗口', function (text) {
                            layero.find('.layui-layer-btn').append('<div class="checkbox sr-layer-btn-cb" myIframeId="layer_' + dfop.id + '" ><label><input checked="checked" type="checkbox" >' + text + '</label></div>');
                        });
                        layero.find('.layui-layer-btn a').each(function () {
                            var $this = $(this);
                            var _text = $this.text();
                            top.srAdmin.language.get(_text, function (text) {
                                $this.text(text);
                            });

                        });
                    }
                    layero.find('.layui-layer-title').each(function () {
                        var $this = $(this);
                        var _text = $this.text();
                        top.srAdmin.language.get(_text, function (text) {
                            $this.text(text);
                        });

                    });
                },
                yes: function (index) {
                    var flag = true;
                    if (!!dfop.callBack) {
                        flag = dfop.callBack('layer_' + dfop.id);
                    }
                    if (!!flag) {
                        srAdmin.layerClose('', index);
                    }
                },
                end: function () {
                    top['layer_' + dfop.id] = null;
                    if (!!dfop.end) {
                        dfop.end();
                    }
                }
            });
        },
        // 关闭弹层
        layerClose: function (name, index) {
            var _index;
            if (!!index) {
                _index = index;
            }
            else {
                _index = top.layer.getFrameIndex(name);
            }
            var layero = top.$("#layui-layer" + _index);
            var $IsClose = layero.find('.layui-layer-btn').find(".sr-layer-btn-cb input");
            var IsClose = $IsClose.is(":checked");
            if ($IsClose.length == 0) {
                IsClose = true;
            }
            if (IsClose) {
                top.layer.close(_index); //再执行关闭  
            } else {
                top[layero[0].srAdmin_layerid].location.reload();
            }

        }
    });


})(window.jQuery, top.srAdmin);


////////////////////////////////////////////////////////////


/*
 * @Describe：ajax操作方法
 */
;(function ($, srAdmin) {
    "use strict";
    var httpCode = {
        success: 200,
        fail: 400,
        exception: 500,
        nologin: 410 // 没有登录者信息
    };
    var exres = { code: httpCode.exception, info: '通信异常，请联系管理员！' }
    $.extend(srAdmin, {
        // http 通信异常的时候调用此方法
        httpErrorLog: function (msg) {
            srAdmin.log(msg);
        },
        // http请求返回数据码
        httpCode: httpCode,
        // get请求方法（异步）:url地址,callback回调函数
        httpAsyncGet: function (url, callback) {
            var loginInfo = srAdmin.clientdata.get(['userinfo']);
            var account = '';
            if (loginInfo) {
                account = loginInfo.account;
            }

            $.ajax({
                url: url,
                headers: { account: account },
                type: "GET",
                dataType: "json",
                async: true,
                cache: false,
                success: function (res) {
                    if (res.code == srAdmin.httpCode.nologin) {
                        var _topUrl = top.$.rootUrl + '/Login/Index';
                        switch (res.info) {
                            case 'nologin':
                                break;
                            case 'noip':
                                _topUrl += '?error=ip';
                                break;
                            case 'notime':
                                _topUrl += '?error=time';
                                break;
                            case 'other':
                                _topUrl += '?error=other';
                                break;
                        }
                        top.window.location.href = _topUrl;
                        callback(res);
                        return;
                    }

                    if (res.code == srAdmin.httpCode.exception) {
                        srAdmin.httpErrorLog(res.info);
                        res.info = '系统异常，请联系管理员！';
                    }
                    callback(res);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    srAdmin.httpErrorLog(textStatus);
                    callback(exres);
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        },
        // get请求方法（同步）:url地址,param参数
        httpGet: function (url, param) {
            var loginInfo = srAdmin.clientdata.get(['userinfo']);
            var account = '';
            if (loginInfo) {
                account = loginInfo.account;
            }

            var _res = {};
            $.ajax({
                url: url,
                headers: { account: account },
                data: param,
                type: "GET",
                dataType: "json",
                async: false,
                cache: false,
                success: function (res) {
                    if (res.code == srAdmin.httpCode.nologin) {
                        var _topUrl = top.$.rootUrl + '/Login/Index';
                        switch (res.info) {
                            case 'nologin':
                                break;
                            case 'noip':
                                _topUrl += '?error=ip';
                                break;
                            case 'notime':
                                _topUrl += '?error=time';
                                break;
                            case 'other':
                                _topUrl += '?error=other';
                                break;
                        }
                        top.window.location.href = _topUrl;
                        return {};
                    }

                    if (res.code == srAdmin.httpCode.exception) {
                        srAdmin.httpErrorLog(res.info);
                        res.info = '系统异常，请联系管理员！';
                    }
                    _res = res;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    srAdmin.httpErrorLog(textStatus);
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
            return _res;
        },
        // post请求方法（异步）:url地址,param参数,callback回调函数
        httpAsyncPost: function (url, param, callback) {
            var loginInfo = srAdmin.clientdata.get(['userinfo']);
            var account = '';
            if (loginInfo) {
                account = loginInfo.account;
            }

            $.ajax({
                url: url,
                headers: { account: account },
                data: param,
                type: "POST",
                dataType: "json",
                async: true,
                cache: false,
                success: function (res) {
                    if (res.code == srAdmin.httpCode.nologin) {
                        var _topUrl = top.$.rootUrl + '/Login/Index';
                        switch (res.info) {
                            case 'nologin':
                                break;
                            case 'noip':
                                _topUrl += '?error=ip';
                                break;
                            case 'notime':
                                _topUrl += '?error=time';
                                break;
                            case 'other':
                                _topUrl += '?error=other';
                                break;
                        }
                        top.window.location.href = _topUrl;
                        callback(res);
                        return;
                    }

                    if (res.code == srAdmin.httpCode.exception) {
                        srAdmin.httpErrorLog(res.info);
                        res.info = '系统异常，请联系管理员！';
                    }
                    callback(res);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    srAdmin.httpErrorLog(textStatus);
                    callback(exres);
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        },
        // post请求方法（同步步）:url地址,param参数,callback回调函数
        httpPost: function (url, param, callback) {
            var loginInfo = srAdmin.clientdata.get(['userinfo']);
            var account = '';
            if (loginInfo) {
                account = loginInfo.account;
            }

            $.ajax({
                url: url,
                headers: { account: account },
                data: param,
                type: "POST",
                dataType: "json",
                async: false,
                cache: false,
                success: function (res) {
                    if (res.code == srAdmin.httpCode.nologin) {
                        var _topUrl = top.$.rootUrl + '/Login/Index';
                        switch (res.info) {
                            case 'nologin':
                                break;
                            case 'noip':
                                _topUrl += '?error=ip';
                                break;
                            case 'notime':
                                _topUrl += '?error=time';
                                break;
                            case 'other':
                                _topUrl += '?error=other';
                                break;
                        }
                        top.window.location.href = _topUrl;
                        callback(res);
                        return;
                    }


                    if (res.code == srAdmin.httpCode.exception) {
                        srAdmin.httpErrorLog(res.info);
                        res.info = '系统异常，请联系管理员！';
                    }
                    callback(res);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    srAdmin.httpErrorLog(textStatus);
                    callback(exres);
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        },
        // ajax 异步封装
        httpAsync: function (type, url, param, callback) {
            var loginInfo = srAdmin.clientdata.get(['userinfo']);
            var account = '';
            if (loginInfo) {
                account = loginInfo.account;
            }
            $.ajax({
                url: url,
                headers: { account: account },
                data: param,
                type: type,
                dataType: "json",
                async: true,
                cache: false,
                success: function (res) {
                    if (res.code == srAdmin.httpCode.nologin) {
                        var _topUrl = top.$.rootUrl + '/Login/Index';
                        switch (res.info) {
                            case 'nologin':
                                break;
                            case 'noip':
                                _topUrl += '?error=ip';
                                break;
                            case 'notime':
                                _topUrl += '?error=time';
                                break;
                            case 'other':
                                _topUrl += '?error=other';
                                break;
                        }
                        top.window.location.href = _topUrl;
                        callback(null);
                        return;
                    }

                    if (res.code == srAdmin.httpCode.success) {
                        callback(res.data);
                    }
                    else {
                        srAdmin.httpErrorLog(res.info);
                        callback(null);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    srAdmin.httpErrorLog(textStatus);
                    callback(null);
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        },

        deleteForm: function (url, param, callback) {
            srAdmin.loading(true, '正在删除数据');
            srAdmin.httpAsyncPost(url, param, function (res) {
                srAdmin.loading(false);
                if (res.code == srAdmin.httpCode.success) {
                    if (!!callback) {
                        callback(res);
                    }
                    srAdmin.alert.success(res.info);
                }
                else {
                    srAdmin.alert.error(res.info);
                    srAdmin.httpErrorLog(res.info);
                }
                layer.close(layer.index);
            });
        },
        postForm: function (url, param, callback) {
            srAdmin.loading(true, '正在提交数据');
            srAdmin.httpAsyncPost(url, param, function (res) {
                srAdmin.loading(false);
                if (res.code == srAdmin.httpCode.success) {
                    if (!!callback) {
                        callback(res);
                    }
                    srAdmin.alert.success(res.info);
                }
                else {
                    srAdmin.alert.error(res.info);
                    srAdmin.httpErrorLog(res.info);
                }
                layer.close(layer.index);
            });
        }
    });

})(window.jQuery, top.srAdmin);



////////////////////////////////////////////////////////////


/*
 * @Describe：获取客户端数据
 */
;(function ($, srAdmin) {
    "use strict";

    var loadSate = {
        no: -1,  // 还未加载
        yes: 1,  // 已经加载成功
        ing: 0,  // 正在加载中
        fail: 2  // 加载失败
    };

    var clientDataFn = {};
    var clientAsyncData = {};

    var clientData = {};


    function initLoad(callback) {
        var res = loadSate.yes;
        for (var id in clientDataFn) {
            var _fn = clientDataFn[id];
            if (_fn.state == loadSate.fail) {
                res = loadSate.fail;
                break;
            }
            else if (_fn.state == loadSate.no) {
                res = loadSate.ing;
                _fn.init();
            }
            else if (_fn.state == loadSate.ing) {
                res = loadSate.ing;
            }
        }
        if (res == loadSate.yes) {
            callback(true);
        } else if (res == loadSate.fail) {
            callback(false);
        }
        else {
            setTimeout(function () {
                initLoad(callback);
            }, 100);
        }
    }
    function get(key, data) {
        var res = "";
        var len = data.length;
        if (len == undefined) {
            res = data[key];
        }
        else {
            for (var i = 0; i < len; i++) {
                if (key(data[i])) {
                    res = data[i];
                    break;
                }
            }
        }
        return res;
    }

    srAdmin.clientdata = {
        init: function (callback) {
            initLoad(function (res) {
                callback(res);
                if (res) {// 开始异步加载数据
                    clientAsyncData.company.init();
                }
            });
        },
        get: function (nameArray) {//[key,function (v) { return v.key == value }]
            var res = "";
            if (!nameArray) {
                return res;
            }
            var len = nameArray.length;
            var data = clientData;
            for (var i = 0; i < len; i++) {
                res = get(nameArray[i], data);
                if (res != "" && res != undefined) {
                    data = res;
                }
                else {
                    break;
                }
            }
            res = res || "";
            return res;
        },
        getAsync: function (name, op) {//
            return clientAsyncData[name].get(op);
        },
        getAllAsync: function (name, op) {//
            return clientAsyncData[name].getAll(op);
        },
        getsAsync: function (name, op) {//
            return clientAsyncData[name].gets(op);
        },
        update: function (name) {
            clientAsyncData[name].update && clientAsyncData[name].update();
        }
    };


    /*******************登录后数据***********************/
    // 注册数据的加载方法
    // 功能模块数据
    clientDataFn.modules = {
        state: loadSate.no,
        init: function () {
            //初始化加载数据
            clientDataFn.modules.state = loadSate.ing;
            srAdmin.httpAsyncGet($.rootUrl + '/BasicModule/Module/GetModuleList', function (res) {
                if (res.code == srAdmin.httpCode.success) {
                    clientData.modules = res.data;
                    clientDataFn.modules.toMap();
                    clientDataFn.modules.state = loadSate.yes;
                }
                else {
                    clientData.modules = [];
                    clientDataFn.modules.toMap();
                    clientDataFn.modules.state = loadSate.fail;
                }
            });
        },
        toMap: function () {
            //转化成树结构 和 转化成字典结构
            var modulesTree = {};
            var modulesMap = {};
            var _len = clientData.modules.length;
            for (var i = 0; i < _len; i++) {
                var _item = clientData.modules[i];
                if (_item.F_EnabledMark == 1) {
                    modulesTree[_item.F_ParentId] = modulesTree[_item.F_ParentId] || [];
                    modulesTree[_item.F_ParentId].push(_item);
                    modulesMap[_item.F_ModuleId] = _item;
                }
            }
            clientData.modulesTree = modulesTree;
            clientData.modulesMap = modulesMap;
        }
    };
    // 登录用户信息
    clientDataFn.userinfo = {
        state: loadSate.no,
        init: function () {
            //初始化加载数据
            clientDataFn.userinfo.state = loadSate.ing;
            srAdmin.httpAsyncGet($.rootUrl + '/Login/GetUserInfo', function (res) {
                if (res.code == srAdmin.httpCode.success) {
                    clientData.userinfo = res.data;
                    clientDataFn.userinfo.state = loadSate.yes;
                }
                else {
                    clientDataFn.userinfo.state = loadSate.fail;
                }
            });
        }
    };

    /*******************使用时异步获取*******************/
    var storage = {
        get: function (name) {
            if (localStorage) {
                return JSON.parse(localStorage.getItem(name)) || {};
            }
            else {
                return clientData[name] || {};
            }
        },
        set: function (name, data) {
            if (localStorage) {
                localStorage.setItem(name, JSON.stringify(data));
            }
            else {
                clientData[name] = data;
            }
        }
    };
    // 公司信息
    clientAsyncData.company = {
        states: loadSate.no,
        init: function () {
            if (clientAsyncData.company.states == loadSate.no) {
                clientAsyncData.company.states = loadSate.ing;
                var ver = storage.get("companyData").ver || "";
                srAdmin.httpAsync('GET', top.$.rootUrl + '/OrganizationModule/Company/GetMap', { ver: ver }, function (data) { 
                    if (!data) {
                        clientAsyncData.company.states = loadSate.fail;
                    } else {
                        if (data.ver) {
                            storage.set("companyData", data);
                        }
                        clientAsyncData.company.states = loadSate.yes;
                        clientAsyncData.department.init();
                    }
                });
            }
        },
        get: function (op) {
            clientAsyncData.company.init();
            if (clientAsyncData.company.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.company.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("companyData").data || {};
                op.callback(data[op.key] || {}, op);
            }
        },
        getAll: function (op) {
            clientAsyncData.company.init();
            if (clientAsyncData.company.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.company.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("companyData").data || {};
                op.callback(data, op);
            }
        }
    };
    // 部门信息
    clientAsyncData.department = {
        states: loadSate.no,
        init: function () {
            if (clientAsyncData.department.states == loadSate.no) {
                clientAsyncData.department.states = loadSate.ing;
                var ver = storage.get("departmentData").ver || "";
                srAdmin.httpAsync('GET', top.$.rootUrl + '/OrganizationModule/Department/GetMap', { ver: ver }, function (data) {
                    if (!data) {
                        clientAsyncData.department.states = loadSate.fail;
                    } else {
                        if (data.ver) {
                            storage.set("departmentData", data);
                        }
                        clientAsyncData.department.states = loadSate.yes;
                        clientAsyncData.user.init();
                    }
                });
            }
        },
        get: function (op) {
            clientAsyncData.department.init();
            if (clientAsyncData.department.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.department.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("departmentData").data || {};
                op.callback(data[op.key] || {}, op);
            }
        },
        getAll: function (op) {
            clientAsyncData.department.init();
            if (clientAsyncData.department.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.department.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("departmentData").data || {};
                op.callback(data, op);
            }
        }
    };
    // 人员信息
    clientAsyncData.user = {
        states: loadSate.no,
        init: function () {
            if (clientAsyncData.user.states == loadSate.no) {
                clientAsyncData.user.states = loadSate.ing;
                var ver = storage.get("userData").ver || "";
                srAdmin.httpAsync('GET', top.$.rootUrl + '/OrganizationModule/User/GetMap', { ver: ver }, function (data) {
                    if (!data) {
                        clientAsyncData.user.states = loadSate.fail;
                    } else {
                        if (data.ver) {
                            storage.set("userData", data);
                        }
                        clientAsyncData.user.states = loadSate.yes;
                        clientAsyncData.dataItem.init();
                    }
                });
            }
        },
        get: function (op) {
            clientAsyncData.user.init();
            if (clientAsyncData.user.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.user.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("userData").data || {};
                op.callback(data[op.key] || {}, op);
            }
        },
        gets: function (op) {
            clientAsyncData.user.init();
            if (clientAsyncData.user.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.user.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("userData").data || {};
                if (op.key == undefined || op.key == null) {
                    op.callback('', op);
                }
                else {
                    op.key = op.key + '';
                    var keyList = op.key.split(',');
                    var _text = []
                    $.each(keyList, function (_index, _item) {
                        var _user = data[_item];
                        if (_user) {
                            _text.push(_user.name);
                        }
                    });
                    op.callback(String(_text), op);
                }
            }
        },
        getAll: function (op) {
            clientAsyncData.user.init();
            if (clientAsyncData.user.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.user.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("userData").data || {};
                op.callback(data, op);
            }
        }
    };
    // 数据字典
    clientAsyncData.dataItem = {
        states: loadSate.no,
        init: function () {
            if (clientAsyncData.dataItem.states == loadSate.no) {
                clientAsyncData.dataItem.states = loadSate.ing;
                var ver = storage.get("dataItemData").ver || "";
                srAdmin.httpAsync('GET', top.$.rootUrl + '/BasicModule/DataItem/GetMap', { ver: ver }, function (data) {
                    if (!data) {
                        clientAsyncData.dataItem.states = loadSate.fail;
                    } else {
                        if (data.ver) {
                            storage.set("dataItemData", data);
                        }
                        clientAsyncData.dataItem.states = loadSate.yes;
                        clientAsyncData.db.init();
                    }
                });
            }
        },
        get: function (op) {
            clientAsyncData.dataItem.init();
            if (clientAsyncData.dataItem.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.dataItem.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("dataItemData").data || {};

                // 数据字典翻译
                var _item = clientAsyncData.dataItem.find(op.key, data[op.code] || {});
                if (_item) {
                    top.srAdmin.language.get(_item.text, function (text) {
                        _item.text = text;
                        op.callback(_item, op);
                    });
                }
                else {
                    op.callback({}, op);
                }
            }
        },
        getAll: function (op) {
            clientAsyncData.dataItem.init();
            if (clientAsyncData.dataItem.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.dataItem.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("dataItemData").data || {};
                var res = {};
                $.each(data[op.code] || {}, function (_index, _item) {
                    _item.text = top.srAdmin.language.getSyn(_item.text);
                    res[_index] = _item;
                });
                op.callback(res, op);
            }
        },
        gets: function (op) {
            clientAsyncData.dataItem.init();
            if (clientAsyncData.dataItem.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.dataItem.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("dataItemData").data || {};
                if (op.key == undefined || op.key == null) {
                    op.callback('', op);
                }
                else {
                    op.key = op.key + '';
                    var keyList = op.key.split(',');
                    var _text = []
                    $.each(keyList, function (_index, _item) {
                        var _item = clientAsyncData.dataItem.find(_item, data[op.code] || {});
                        top.srAdmin.language.get(_item.text, function (text) {
                            _text.push(text);
                        });
                    });
                    op.callback(String(_text), op);
                }
            }
        },
        find: function (key, data) {
            var res = {};
            for (var id in data) {
                if (data[id].value == key) {
                    res = data[id];


                    break;
                }
            }
            return res;
        },
        update: function () {
            clientAsyncData.dataItem.states = loadSate.no;
            clientAsyncData.dataItem.init();
        }
    };
    // 数据库连接数据
    clientAsyncData.db = {
        states: loadSate.no,
        init: function () {
            if (clientAsyncData.db.states == loadSate.no) {
                clientAsyncData.db.states = loadSate.ing;
                var ver = storage.get("dbData").ver || "";
                srAdmin.httpAsync('GET', top.$.rootUrl + '/BasicModule/DatabaseLink/GetMap', { ver: ver }, function (data) {
                    if (!data) {
                        clientAsyncData.db.states = loadSate.fail;
                    } else {
                        if (data.ver) {
                            storage.set("dbData", data);
                        }
                        clientAsyncData.db.states = loadSate.yes;
                    }
                });
            }
        },
        get: function (op) {
            clientAsyncData.db.init();
            if (clientAsyncData.db.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.db.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("dbData").data || {};
                op.callback(data[op.key] || {}, op);
            }
        },
        getAll: function (op) {
            clientAsyncData.db.init();
            if (clientAsyncData.db.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.db.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("dbData").data || {};
                op.callback(data, op);
            }
        }
    };
    // 数据源数据
    clientAsyncData.sourceData = {
        states: {},
        get: function (op) {
            if (clientAsyncData.sourceData.states[op.code] == undefined || clientAsyncData.sourceData.states[op.code] == loadSate.no) {
                clientAsyncData.sourceData.states[op.code] = loadSate.ing;
                clientAsyncData.sourceData.load(op.code);
            }

            if (clientAsyncData.sourceData.states[op.code] == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.sourceData.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("sourceData_" + op.code).data || [];
                if (!!data) {
                    op.callback(clientAsyncData.sourceData.find(op.key, op.keyId, data) || {}, op);
                } else {
                    op.callback({}, op);
                }
            }
        },
        getAll: function (op) {
            if (clientAsyncData.sourceData.states[op.code] == undefined || clientAsyncData.sourceData.states[op.code] == loadSate.no) {
                clientAsyncData.sourceData.states[op.code] = loadSate.ing;
                clientAsyncData.sourceData.load(op.code);
            }

            if (clientAsyncData.sourceData.states[op.code] == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.sourceData.getAll(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else if (clientAsyncData.sourceData.states[op.code] == loadSate.yes) {
                var data = storage.get("sourceData_" + op.code).data || [];

                if (!!data) {
                    op.callback(data, op);
                } else {
                    op.callback({}, op);
                }
            }
        },
        load: function (code) {
            var ver = storage.get("sourceData_" + code).ver || "";
            srAdmin.httpAsync('GET', top.$.rootUrl + '/BasicModule/DataSource/GetMap', { code: code, ver: ver }, function (data) {
                if (!data) {
                    clientAsyncData.sourceData.states[code] = loadSate.fail;
                } else {
                    if (data.ver) {
                        storage.set("sourceData_" + code, data);
                    }
                    clientAsyncData.sourceData.states[code] = loadSate.yes;
                }
            });
        },
        find: function (key, keyId, data) {
            var res = {};
            for (var i = 0, l = data.length; i < l; i++) {
                if (data[i][keyId] == key) {
                    res = data[i];
                    break;
                }
            }
            return res;
        }
    };
    // 获取自定义数据 url key valueId
    clientAsyncData.custmerData = {
        states: {},
        get: function (op) {
            if (clientAsyncData.custmerData.states[op.url] == undefined || clientAsyncData.custmerData.states[op.url] == loadSate.no) {
                clientAsyncData.custmerData.states[op.url] = loadSate.ing;
                clientAsyncData.custmerData.load(op.url);
            }
            if (clientAsyncData.custmerData.states[op.url] == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.custmerData.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = clientData[op.url] || [];
                if (!!data) {
                    op.callback(clientAsyncData.custmerData.find(op.key, op.keyId, data) || {}, op);
                } else {
                    op.callback({}, op);
                }
            }
        },
        gets: function (op) {
            if (clientAsyncData.custmerData.states[op.url] == undefined || clientAsyncData.custmerData.states[op.url] == loadSate.no) {
                clientAsyncData.custmerData.states[op.url] = loadSate.ing;
                clientAsyncData.custmerData.load(op.url);
            }
            if (clientAsyncData.custmerData.states[op.url] == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.custmerData.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = clientData[op.url] || [];
                if (!!data) {
                    if (op.key == undefined || op.key == null) {
                        op.callback('', op);
                    }
                    else {
                        op.key = op.key + '';
                        var keyList = op.key.split(',');
                        var _text = []
                        $.each(keyList, function (_index, _item) {
                            var _item = clientAsyncData.custmerData.find(op.key, op.keyId, data) || {};
                            if (_item[op.textId]) {
                                _text.push(_item[op.textId]);
                            }

                        });
                        op.callback(String(_text), op);
                    }


                } else {
                    op.callback('', op);
                }
            }
        },
        getAll: function (op) {
            if (clientAsyncData.custmerData.states[op.url] == undefined || clientAsyncData.custmerData.states[op.url] == loadSate.no) {
                clientAsyncData.custmerData.states[op.url] = loadSate.ing;
                clientAsyncData.custmerData.load(op.url);
            }
            if (clientAsyncData.custmerData.states[op.url] == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.custmerData.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = clientData[op.url] || [];
                if (!!data) {
                    op.callback(data, op);
                } else {
                    op.callback([], op);
                }
            }
        },
        load: function (url) {
            srAdmin.httpAsync('GET', top.$.rootUrl + url, {}, function (data) {
                if (!!data) {
                    clientData[url] = data;
                }
                clientAsyncData.custmerData.states[url] = loadSate.yes;
            });
        },
        find: function (key, keyId, data) {
            var res = {};
            for (var i = 0, l = data.length; i < l; i++) {
                if (data[i][keyId] == key) {
                    res = data[i];
                    break;
                }
            }
            return res;
        }
    };

    // 获取桌面配置数据
    clientAsyncData.desktop = {
        states: loadSate.no,
        init: function () {
            if (clientAsyncData.desktop.states == loadSate.no) {
                clientAsyncData.desktop.states = loadSate.ing;
                var ver = storage.get("desktopData").ver || "";
                srAdmin.httpAsync('GET', top.$.rootUrl + '/SR_Desktop/DTTarget/GetMap', { ver: ver }, function (data) {
                    if (!data) {
                        clientAsyncData.desktop.states = loadSate.fail;
                    } else {
                        if (data.ver) {
                            storage.set("desktopData", data);
                        }
                        clientAsyncData.desktop.states = loadSate.yes;
                        clientAsyncData.department.init();
                    }
                });
            }
        },
        get: function (op) {
            clientAsyncData.desktop.init();
            if (clientAsyncData.desktop.states == loadSate.ing) {
                setTimeout(function () {
                    clientAsyncData.desktop.get(op);
                }, 100);// 如果还在加载100ms后再检测
            }
            else {
                var data = storage.get("desktopData").data || {};
                op.callback(data || {}, op);
            }
        }
    };

})(window.jQuery, top.srAdmin);

////////////////////////////////////////////////////////////

/*
 * @Describe：客户端语言包加载
 */

;(function ($, srAdmin) {
    "use strict";

    srAdmin.language = {
        getMainCode: function () {
            return mainType;
        },
        get: function (text, callback) {
            callback(text);
        },
        getSyn: function (text) {
            return text;
        }
    };

})(window.jQuery, top.srAdmin);


////////////////////////////////////////////////////////////

/*
 * tree菜单	
 */
(function ($, srAdmin) {
    "use strict";
    $.srtree = {
        getItem: function (path, dfop) {
            var ap = path.split(".");
            var t = dfop.data;
            for (var i = 0; i < ap.length; i++) {
                if (i == 0) {
                    t = t[ap[i]];
                }
                else {
                    t = t.ChildNodes[ap[i]];
                }
            }
            return t;
        },
        render: function ($self) {
            var dfop = $self[0]._srtree.dfop;
            // 渲染成树
            var $treeRoot = $('<ul class="sr-tree-root" ></ul>');
            var _len = dfop.data.length;
            for (var i = 0; i < _len; i++) {
                var $node = $.srtree.renderNode(dfop.data[i], 0, i, dfop);
                $treeRoot.append($node);
            }
            $self.append($treeRoot);
            $self.srscroll();
            dfop = null;
        },
        renderNode: function (node, deep, path, dfop) {
            if (node.shide) {
                return "";
            }

            node._deep = deep;
            node._path = path;
            // 渲染成单个节点
            var nid = node.id.replace(/[^\w]/gi, "_");
            var title = node.title || node.text;
            var $node = $('<li class="sr-tree-node"></li>');
            var $nodeDiv = $('<div id="' + dfop.id + '_' + nid + '" tpath="' + path + '" title="' + title + '"  dataId="' + dfop.id + '"  class="sr-tree-node-el" ></div>');
            if (node.hasChildren) {
                var c = (node.isexpand || dfop.isAllExpand) ? 'sr-tree-node-expanded' : 'sr-tree-node-collapsed';
                $nodeDiv.addClass(c);
            }
            else {
                $nodeDiv.addClass('sr-tree-node-leaf');
            }
            // span indent
            var $span = $('<span class="sr-tree-node-indent"></span>');
            if (deep == 1) {
                $span.append('<img class="sr-tree-icon" src="' + dfop.cbiconpath + 's.gif"/>');
            }
            else if (deep > 1) {
                $span.append('<img class="sr-tree-icon" src="' + dfop.cbiconpath + 's.gif"/>');
                for (var j = 1; j < deep; j++) {
                    $span.append('<img class="sr-tree-icon" src="' + dfop.cbiconpath + 's.gif"/>');
                }
            }
            $nodeDiv.append($span);
            // img
            var $img = $('<img class="sr-tree-ec-icon" src="' + dfop.cbiconpath + 's.gif"/>');
            $nodeDiv.append($img);
            // checkbox
            if (node.showcheck) {
                if (node.checkstate == null || node.checkstate == undefined) {
                    node.checkstate = 0;
                }
                var $checkBox = $('<img  id="' + dfop.id + '_' + nid + '_cb" + class="sr-tree-node-cb" src="' + dfop.cbiconpath + dfop.icons[node.checkstate] + '" />');
                $nodeDiv.append($checkBox);
            }
            // 显示的小图标
            if (node.icon != -1) {
                if (!!node.icon) {
                    $nodeDiv.append('<i class="' + node.icon + '"></i>&nbsp;');
                } else if (node.hasChildren) {
                    if (node.isexpand || dfop.isAllExpand) {
                        $nodeDiv.append('<i class="fa fa-folder-open" style="width:15px">&nbsp;</i>');
                    }
                    else {
                        $nodeDiv.append('<i class="fa fa-folder" style="width:15px">&nbsp;</i>');
                    }
                }
                else {
                    $nodeDiv.append('<i class="fa fa-file-o"></i>&nbsp;');
                }
            }
            // a
            var ahtml = '<a class="sr-tree-node-anchor" href="javascript:void(0);">';
            ahtml += '<span data-value="' + node.id + '" class="sr-tree-node-text" >' + node.text + '</span>';
            ahtml += '</a>';
            $nodeDiv.append(ahtml);
            // 节点事件绑定
            $nodeDiv.on('click', $.srtree.nodeClick);

            if (!node.complete) {
                $nodeDiv.append('<div class="sr-tree-loading"><img class="sr-tree-ec-icon" src="' + dfop.cbiconpath + 'loading.gif"/></div>');
            }

            $node.append($nodeDiv);
            if (node.hasChildren) {
                var $treeChildren = $('<ul  class="sr-tree-node-ct" >');
                if (!node.isexpand && !dfop.isAllExpand) {
                    $treeChildren.css('display', 'none');
                }
                if (node.ChildNodes) {
                    var l = node.ChildNodes.length;
                    for (var k = 0; k < l; k++) {
                        node.ChildNodes[k].parent = node;
                        var $childNode = $.srtree.renderNode(node.ChildNodes[k], deep + 1, path + "." + k, dfop);
                        $treeChildren.append($childNode);
                    }
                    $node.append($treeChildren);
                }
            }
            node.render = true;
            dfop = null;
            return $node;
        },
        renderNodeAsync: function ($this, node, dfop) {
            var $treeChildren = $('<ul  class="sr-tree-node-ct" >');
            if (!node.isexpand && !dfop.isAllExpand) {
                $treeChildren.css('display', 'none');
            }
            if (node.ChildNodes) {
                var l = node.ChildNodes.length;
                for (var k = 0; k < l; k++) {
                    node.ChildNodes[k].parent = node;
                    var $childNode = $.srtree.renderNode(node.ChildNodes[k], node._deep + 1, node._path + "." + k, dfop);
                    $treeChildren.append($childNode);
                }
                $this.parent().append($treeChildren);
            }
            return $treeChildren;
        },
        renderToo: function ($self) {
            var dfop = $self[0]._srtree.dfop;
            // 渲染成树
            var $treeRoot = $self.find('.sr-tree-root');
            $treeRoot.html('');
            var _len = dfop.data.length;
            for (var i = 0; i < _len; i++) {
                var $node = $.srtree.renderNode(dfop.data[i], 0, i, dfop);
                $treeRoot.append($node);
            }
            dfop = null;
        },
        nodeClick: function (e) {
            var et = e.target || e.srcElement;
            var $this = $(this);
            var $parent = $('#' + $this.attr('dataId'));
            var dfop = $parent[0]._srtree.dfop;
            if (et.tagName == 'IMG') {
                var $et = $(et);
                var $ul = $this.next('.sr-tree-node-ct');
                if ($et.hasClass("sr-tree-ec-icon")) {
                    if ($this.hasClass('sr-tree-node-expanded')) {
                        $ul.slideUp(200, function () {
                            $this.removeClass('sr-tree-node-expanded');
                            $this.addClass('sr-tree-node-collapsed');
                        });
                    }
                    else if ($this.hasClass('sr-tree-node-collapsed')) {
                        // 展开
                        var path = $this.attr('tpath');
                        var node = $.srtree.getItem(path, dfop);
                        if (!node.complete) {
                            if (!node._loading) {
                                node._loading = true;// 表示正在加载数据
                                $this.find('.sr-tree-loading').show();
                                srAdmin.httpAsync('GET', dfop.url, { parentId: node.id }, function (data) {
                                    if (!!data) {
                                        node.ChildNodes = data;
                                        $ul = $.srtree.renderNodeAsync($this, node, dfop);
                                        $ul.slideDown(200, function () {
                                            $this.removeClass('sr-tree-node-collapsed');
                                            $this.addClass('sr-tree-node-expanded');
                                        });
                                        node.complete = true;
                                        $this.find('.sr-tree-loading').hide();
                                    }
                                    node._loading = false;
                                });
                            }
                        }
                        else {
                            $ul.slideDown(200, function () {
                                $this.removeClass('sr-tree-node-collapsed');
                                $this.addClass('sr-tree-node-expanded');
                            });
                        }
                    }

                }
                else if ($et.hasClass("sr-tree-node-cb")) {
                    var path = $this.attr('tpath');
                    var node = $.srtree.getItem(path, dfop);

                    if (node.checkstate == 1) {
                        node.checkstate = 0;
                    }
                    else {
                        node.checkstate = 1;
                    }
                    $et.attr('src', dfop.cbiconpath + dfop.icons[node.checkstate]);
                    $.srtree.checkChild($.srtree.check, node, node.checkstate, dfop);
                    $.srtree.checkParent($.srtree.check, node, node.checkstate, dfop);
                    if (!!dfop.nodeCheck) {
                        dfop.nodeCheck(node, $this);
                    }
                }
            }
            else {
                var path = $this.attr('tpath');
                var node = $.srtree.getItem(path, dfop);
                dfop.currentItem = node;
                $('#' + dfop.id).find('.sr-tree-selected').removeClass('sr-tree-selected');
                $this.addClass('sr-tree-selected');
                if (!!dfop.nodeClick) {
                    dfop.nodeClick(node, $this);
                }
            }
            return false;
        },
        check: function (item, state, type, dfop) {
            var pstate = item.checkstate;
            if (type == 1) {
                item.checkstate = state;
            }
            else {// go to childnodes
                var cs = item.ChildNodes;
                var l = cs.length;
                var ch = true;
                for (var i = 0; i < l; i++) {
                    cs[i].checkstate = cs[i].checkstate || 0;
                    if ((state == 1 && cs[i].checkstate != 1) || state == 0 && cs[i].checkstate != 0) {
                        ch = false;
                        break;
                    }
                }
                if (ch) {
                    item.checkstate = state;
                }
                else {
                    item.checkstate = 2;
                }
            }
            //change show
            if (item.render && pstate != item.checkstate) {
                var nid = item.id.replace(/[^\w]/gi, "_");
                var et = $("#" + dfop.id + "_" + nid + "_cb");
                if (et.length == 1) {
                    et.attr("src", dfop.cbiconpath + dfop.icons[item.checkstate]);
                }
            }
        },
        checkParent: function (fn, node, state, dfop) {
            var p = node.parent;
            while (p) {
                var r = fn(p, state, 0, dfop);
                if (r === false) {
                    break;
                }
                p = p.parent;
            }
        },
        checkChild: function (fn, node, state, dfop) {
            if (fn(node, state, 1, dfop) != false) {
                if (node.ChildNodes != null && node.ChildNodes.length > 0) {
                    var cs = node.ChildNodes;
                    for (var i = 0, len = cs.length; i < len; i++) {
                        $.srtree.checkChild(fn, cs[i], state, dfop);
                    }
                }
            }
        },

        search: function (keyword, data) {
            var res = false;
            $.each(data, function (i, row) {
                var flag = false;

                if (!srAdmin.validator.isNotNull(keyword).code || row.text.indexOf(keyword) != -1) {

                    flag = true;
                }
                if (row.hasChildren) {
                    if ($.srtree.search(keyword, row.ChildNodes)) {
                        flag = true;
                    }
                }
                if (flag) {
                    res = true;
                    row.isexpand = true;
                    row.shide = false;
                }
                else {
                    row.shide = true;
                }
            });
            return res;
        },
        findItem: function (data, id, value) {
            var _item = null;
            _fn(data, id, value);
            function _fn(_cdata, _id, _value) {
                for (var i = 0, l = _cdata.length; i < l; i++) {
                    if (_cdata[i][id] == value) {
                        _item = _cdata[i];
                        return true;
                    }
                    if (_cdata[i].hasChildren && _cdata[i].ChildNodes.length > 0) {
                        if (_fn(_cdata[i].ChildNodes, _id, _value)) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return _item;
        },
        listTotree: function (data, parentId, id, text, value, check) {
            // 只适合小数据计算
            var resdata = [];
            var mapdata = {};
            for (var i = 0, l = data.length; i < l; i++) {
                var item = data[i];
                mapdata[item[parentId]] = mapdata[item[parentId]] || [];
                mapdata[item[parentId]].push(item);
            }
            _fn(resdata, '0');
            function _fn(_data, vparentId) {
                var pdata = mapdata[vparentId] || [];
                for (var j = 0, l = pdata.length; j < l; j++) {
                    var _item = pdata[j];
                    var _point = {
                        id: _item[id],
                        text: _item[text],
                        value: _item[value],
                        showcheck: check,
                        checkstate: false,
                        hasChildren: false,
                        isexpand: false,
                        complete: true,
                        ChildNodes: []
                    };
                    if (_fn(_point.ChildNodes, _item[id])) {
                        _point.hasChildren = true;
                        _point.isexpand = true;
                    }
                    _data.push(_point);
                }
                return _data.length > 0;
            }
            return resdata;
        },
        treeTotree: function (data, id, text, value, check, childId) {
            var resdata = [];
            _fn(resdata, data);
            function _fn(todata, fromdata) {
                for (var i = 0, l = fromdata.length; i < l; i++) {
                    var _item = fromdata[i];
                    var _point = {
                        id: _item[id],
                        text: _item[text],
                        value: _item[value],
                        showcheck: check,
                        checkstate: false,
                        hasChildren: false,
                        isexpand: true,
                        complete: true,
                        ChildNodes: []
                    };
                    if (_item[childId].length > 0) {
                        _point.hasChildren = true;
                        _fn(_point.ChildNodes, _item[childId]);
                    }
                    todata.push(_point);
                }
            }
            return resdata;
        },

        addNode: function ($self, node, Id, index) {// 下一版本完善
            var dfop = $self[0]._srtree.dfop;
            if (!!Id)// 在最顶层
            {
                dfop.data.splice(index, 0, node);
                var $node = $.srtree.renderNode(node, 0, index, dfop);
                if ($self.find('.sr-tree-root>li').length == 0) {
                    $self.find('.sr-tree-root>li').append($node);
                }
                else {
                    $self.find('.sr-tree-root>li').eq(index).before($node);
                }

            }
            else {
                var $parentId = $self.find('#' + dfop.id + '_' + Id);
                var path = $parentId.attr('tpath');
                var $node = $.srtree.renderNode(node, 0, path + '.' + index, dfop);
                if ($parentId.next().children().length == 0) {
                    $parentId.next().children().append($node);
                }
                else {
                    $parentId.next().children().eq(index).before($node);
                }
            }
        },
        setValue: function ($self) {
            var dfop = $self[0]._srtree.dfop;
            if (dfop.data.length == 0) {
                setTimeout(function () {
                    $.srtree.setValue($self);
                }, 100);
            }
            else {
                $self.find('span[data-value="' + dfop._value + '"]').trigger('click');
            }
        }
    };

    $.fn.srtree = function (settings) {
        var dfop = {
            icons: ['checkbox_0.png', 'checkbox_1.png', 'checkbox_2.png'],
            method: "GET",
            url: false,
            param: null,
            /* [{
            id,
            text,
            value,
            showcheck,bool
            checkstate,int
            hasChildren,bool
            isexpand,bool
            complete,bool
            ChildNodes,[]
            }]*/
            data: [],
            isAllExpand: false,
            cbiconpath: top.$.rootUrl + '/Content/images/srAdmintree/',
            // 点击事件（节点信息）,节点$对象
            nodeClick: false,
            // 选中事件（节点信息）,节点$对象
            nodeCheck: false

        };
        $.extend(dfop, settings);
        var $self = $(this);
        dfop.id = $self.attr("id");
        if (dfop.id == null || dfop.id == "") {
            dfop.id = "srAdmintree" + new Date().getTime();
            $self.attr("id", dfop.id);
        }
        $self.html('');
        $self.addClass("sr-tree");
        $self[0]._srtree = { dfop: dfop };
        $self[0]._srtree.dfop.backupData = dfop.data;
        if (dfop.url) {
            srAdmin.httpAsync(dfop.method, dfop.url, dfop.param, function (data) {
                $self[0]._srtree.dfop.data = data || [];
                $self[0]._srtree.dfop.backupData = $self[0]._srtree.dfop.data;
                $.srtree.render($self);
            });
        }
        else {
            $.srtree.render($self);
        }
        // pre load the icons
        if (dfop.showcheck) {
            for (var i = 0; i < 3; i++) {
                var im = new Image();
                im.src = dfop.cbiconpath + dfop.icons[i];
            }
        }
        dfop = null;
        return $self;
    };

    $.fn.srtreeSet = function (name, op) {
        var $self = $(this);
        var dfop = $self[0]._srtree.dfop;
        var getCheck = function (items, buff, fn) {
            for (var i = 0, l = items.length; i < l; i++) {
                if ($self.find('#' + dfop.id + '_' + items[i].id.replace(/-/g, '_')).parent().css('display') != 'none') {
                    (items[i].showcheck == true && (items[i].checkstate == 1 || items[i].checkstate == 2)) && buff.push(fn(items[i]));
                    if (!items[i].showcheck || (items[i].showcheck == true && (items[i].checkstate == 1 || items[i].checkstate == 2))) {
                        if (items[i].ChildNodes != null && items[i].ChildNodes.length > 0) {
                            getCheck(items[i].ChildNodes, buff, fn);
                        }
                    }
                }
            }
        };
        var getCheck2 = function (items, buff, fn) {
            for (var i = 0, l = items.length; i < l; i++) {
                (items[i].showcheck == true && (items[i].checkstate == 1 || items[i].checkstate == 2) && !items[i].hasChildren) && buff.push(fn(items[i]));
                if (!items[i].showcheck || (items[i].showcheck == true && (items[i].checkstate == 1 || items[i].checkstate == 2))) {
                    if (items[i].ChildNodes != null && items[i].ChildNodes.length > 0) {
                        getCheck2(items[i].ChildNodes, buff, fn);
                    }
                }
            }
        };

        var setNoCheck = function (items, buff, fn) {
            for (var i = 0, l = items.length; i < l; i++) {
                if (items[i].showcheck) {
                    items[i].checkstate = 0;
                }
                if (items[i].ChildNodes != null && items[i].ChildNodes.length > 0) {
                    setNoCheck(items[i].ChildNodes);
                }
            }
        };


        switch (name) {
            case 'allNoCheck':
                $self.find('.sr-tree-node-cb').attr('src', dfop.cbiconpath + 'checkbox_0.png');
                setNoCheck(dfop.data);
                break;
            case 'allCheck':
                $self.find('.sr-tree-node-cb[src$="checkbox_0.png"]').each(function () {
                    var $this = $(this);
                    if ($this.parent().parent().find('.sr-tree-node-ct').length == 0) {
                        $this.trigger('click');
                    }
                    $this = null;
                });
                break;
            case 'setCheck':
                var list = op;
                $.each(list, function (id, item) {
                    var $div = $self.find('#' + dfop.id + '_' + item.replace(/-/g, '_'));
                    if ($div.next().length == 0) {
                        $div.find('.sr-tree-node-cb').trigger('click');
                    }
                });
                break;
            case 'setValue':
                dfop._value = op;
                $.srtree.setValue($self);
                break;
            case 'currentItem':
                return dfop.currentItem;
                break;
            case 'getCheckNodesEx':// 只获取最下面的选中元素
                var buff = [];
                getCheck2(dfop.data, buff, function (item) { return item; });
                return buff;
                break;
            case 'getCheckNodes':
                var buff = [];
                getCheck(dfop.data, buff, function (item) { return item; });
                return buff;
                break;
            case 'getCheckNodeIds':
                var buff = [];
                getCheck(dfop.data, buff, function (item) { return item.id; });
                return buff;
                break;
            case 'getCheckNodeIdsByPath':
                var buff = [];
                var pathlist
                getCheck(dfop.data, buff, function (item) { return item.id; });
                return buff;
                break;
            case 'search':
                $.srtree.search(op.keyword, dfop.data);
                if (srAdmin.validator.isNotNull(op.keyword).code) {
                    dfop._isSearch = true;
                }
                else if (dfop._isSearch) {
                    dfop._isSearch = false;
                }
                $.srtree.renderToo($self);
                break;
            case 'refresh':
                $.extend(dfop, op || {});
                if (!!dfop.url) {
                    srAdmin.httpAsync(dfop.method, dfop.url, dfop.param, function (data) {
                        $self[0]._srtree.dfop.data = data || [];
                        $self[0]._srtree.dfop.backupData = $self[0]._srtree.dfop.data;
                        $.srtree.renderToo($self);
                        dfop._isSearch = false;
                    });
                }
                else {
                    $self[0]._srtree.dfop.backupData = $self[0]._srtree.dfop.data;
                    $.srtree.renderToo($self);
                    dfop._isSearch = false;
                }
                break;
            case 'addNode':

                break;
            case 'updateNode':

                break;
        }
    }

})(jQuery, top.srAdmin);



////////////////////////////////////////////////////////////










