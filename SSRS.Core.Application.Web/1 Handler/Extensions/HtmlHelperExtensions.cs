﻿
using System;
using System.IO;
using System.Text;
using System.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using SSRS.Cache.Base;
using SSRS.Cache.Factory;
using SSRS.Util;


namespace SSRS.Core.Application.Web
{
    /// <summary>
    /// 描 述：对HtmlHelper类进行扩展
    /// </summary>
    public static class HtmlHelperExtensions
    {
        private static ICache cache = CacheFactory.CaChe();
         
        #region 徐志强 权限模块
        /// <summary>
        /// 设置当前页面地址
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <returns></returns>
        public static HtmlString SetCurrentUrl(this IHtmlHelper htmlHelper)
        {
            string currentUrl = (string)WebHelper.GetHttpItems("currentUrl");
            return new HtmlString("<script>var srCurrentUrl='" + currentUrl + "';var srModuleButtonList;var srModuleColumnList;var srModule;</script>");
        }
        #endregion


        /// <summary>
        /// 往页面中写入js文件
        /// </summary>
        /// <param name="htmlHelper">需要扩展对象</param>
        /// <param name="jsFiles">文件路径</param>
        /// <returns></returns>
        public static HtmlString AppendJsFile(this HtmlHelper htmlHelper, params string[] jsFiles)
        {
            string jsFile = "";
            foreach (string file in jsFiles)
            {
                if (jsFile != "")
                {
                    jsFile += ",";
                }
                jsFile += file;
            }
            string jsStr = "";
            if (Config.GetValue("JsCompressorCache") == "true")
            {
                jsStr = cache.Read<string>(jsFile, CacheId.jscss);
            }
            if (string.IsNullOrEmpty(jsStr))
            {
                jsStr = JsCssHelper.ReadJSFile(jsFiles);
                cache.Write<string>(jsFile, jsStr, CacheId.jscss);
            }

            StringBuilder content = new StringBuilder();
            string jsFormat = "<script>{0}</script>";

            content.AppendFormat(jsFormat, jsStr);
            return new HtmlString(content.ToString());
        }





        /// <summary>
        /// 往页面中写入css样式
        /// </summary>
        /// <param name="htmlHelper">需要扩展对象</param>
        /// <param name="cssFiles">文件路径</param>
        /// <returns></returns>
        public static HtmlString AppendCssFile(this HtmlHelper htmlHelper, params string[] cssFiles)
        {
            string cssFile = "";
            foreach (string file in cssFiles)
            {
                if (cssFile != "")
                {
                    cssFile += ",";
                }
                cssFile += file;
            }
            string cssStr = "";
            if (Config.GetValue("JsCompressorCache") == "true")
            {
                cssStr = cache.Read<string>(cssFile, CacheId.jscss);
            }


            if (string.IsNullOrEmpty(cssStr))
            {
                
                Type myType = typeof(Program);
                string url = Path.GetDirectoryName(myType.Assembly.Location);
    

                    cssStr = JsCssHelper.ReadCssFile(cssFiles);
                if (url != "/")
                {
                    cssStr = cssStr.Replace("url(", "url(" + url);
                }


                cache.Write<string>(cssFile, cssStr, CacheId.jscss);
            }
            StringBuilder content = new StringBuilder();
            string cssFormat = "<style>{0}</style>";
            content.AppendFormat(cssFormat, cssStr);
            return new HtmlString(content.ToString());
        }
    }
}