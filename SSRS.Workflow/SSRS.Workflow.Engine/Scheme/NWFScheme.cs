﻿using System.Collections.Generic;

namespace SSRS.Workflow.Engine
{
    /// <summary>
    /// 
    /// Copyright (c) 2013-2018 上善若水
    /// 创建人：上善若水
    /// 日 期：2018.12.09
    /// 描 述：工作流模板模型
    /// </summary>
    public class NWFScheme
    {
        /// <summary>
        /// 节点数据
        /// </summary>
        public List<NWFNodeInfo> nodes { get; set; }
        /// <summary>
        /// 线条数据
        /// </summary>
        public List<NWFLineInfo> lines { get; set; }
        /// <summary>
        /// 流程撤销作废的时候执行的方法
        /// </summary>
        public NWFCloseDo closeDo { get; set; }
    }
}
