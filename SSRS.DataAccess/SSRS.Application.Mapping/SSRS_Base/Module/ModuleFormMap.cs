﻿

using SSRS.PlugIns.Base.SystemModule;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace SSRS.PlugIns.Mapping.LR_System.Module
{
    public class ModuleFormMap : IEntityTypeConfiguration<ModuleFormEntity>
    {
        public void Configure(EntityTypeBuilder<ModuleFormEntity> builder)
        {
            builder.HasKey(p => p.F_ModuleFormId);
        }
    }
}
