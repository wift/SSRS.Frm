﻿

using Microsoft.EntityFrameworkCore;
using SSRS.PlugIns.Base.SystemModule;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 系统功能模块按钮
    /// </summary>
    public class ModuleButtonMap : IEntityTypeConfiguration<ModuleButtonEntity>
    {
        public void Configure(EntityTypeBuilder<ModuleButtonEntity> builder)
        {
            builder.HasKey(p => p.F_ModuleButtonId);
        }
    }
}
