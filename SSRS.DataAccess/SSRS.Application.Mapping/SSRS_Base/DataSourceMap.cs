﻿using SSRS.PlugIns.Base.SystemModule;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 数据源
    /// </summary>
    public class DataSourceMap : IEntityTypeConfiguration<DataSourceEntity>
    {
        public void Configure(EntityTypeBuilder<DataSourceEntity> builder)
        {
            builder.HasKey(p => p.F_Id);
        }
    }
}
