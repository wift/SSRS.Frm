﻿using SSRS.PlugIns.Base.SystemModule;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 图片保存
    /// </summary>
    public class ImgMap : IEntityTypeConfiguration<ImgEntity>
    {
        public void Configure(EntityTypeBuilder<ImgEntity> builder)
        {
            builder.HasKey(p => p.F_Id);
        }
    }
}
