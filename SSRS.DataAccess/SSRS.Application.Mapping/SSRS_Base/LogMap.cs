﻿using SSRS.PlugIns.Base.SystemModule;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 系统日志
    /// </summary>
    public class LogMap : IEntityTypeConfiguration<LogEntity>
    {
        public void Configure(EntityTypeBuilder<LogEntity> builder)
        {
            builder.HasKey(p => p.F_LogId);
        }
    }
}
