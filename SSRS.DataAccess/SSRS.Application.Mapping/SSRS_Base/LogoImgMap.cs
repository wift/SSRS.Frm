﻿
using SSRS.PlugIns.Base.SystemModule;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SSRS.PlugIns.Mapping.LR_System
{
    public class LogoImgMap : IEntityTypeConfiguration<LogoImgEntity>
    {
        public void Configure(EntityTypeBuilder<LogoImgEntity> builder)
        {
            builder.HasKey(p => p.F_Code);
        }
    }
}
