﻿
using Microsoft.EntityFrameworkCore;
using SSRS.PlugIns.Base.SystemModule;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace SSRS.PlugIns.Mapping.LR_System.Database
{
    /// <summary>
    /// 数据库建表草稿类
    /// </summary>
    public class DbDraftMap : IEntityTypeConfiguration<DbDraftEntity>
    {
        public void Configure(EntityTypeBuilder<DbDraftEntity> builder)
        {
            builder.HasKey(p => p.F_Id);
        }
    }
}
