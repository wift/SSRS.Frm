﻿
using Microsoft.EntityFrameworkCore;
using SSRS.PlugIns.Base.SystemModule;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace SSRS.PlugIns.Mapping.LR_System.Database
{
    /// <summary>
    /// 常用字段类
    /// </summary>
    public class DbFieldMap : IEntityTypeConfiguration<DbFieldEntity>
    {
        public void Configure(EntityTypeBuilder<DbFieldEntity> builder)
        {
            builder.HasKey(p => p.F_Id);
        }
    }
}
