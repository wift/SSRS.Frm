﻿


using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SSRS.PlugIns.Base.SystemModule;

namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 编号规则种子
    /// </summary>
    public class CodeRuleSeedMap : IEntityTypeConfiguration<CodeRuleSeedEntity>
    { 
        public void Configure(EntityTypeBuilder<CodeRuleSeedEntity> builder)
        {
            builder.HasKey(p => p.F_RuleSeedId);
        }
    }
}
