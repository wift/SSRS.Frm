﻿


using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SSRS.PlugIns.Base.SystemModule;

namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 自定义查询
    /// </summary>
    public class CustmerQueryMap : IEntityTypeConfiguration<CustmerQueryEntity>
    {
        public void Configure(EntityTypeBuilder<CustmerQueryEntity> builder)
        {
            builder.HasKey(p => p.F_CustmerQueryId);
        }
    }
}
