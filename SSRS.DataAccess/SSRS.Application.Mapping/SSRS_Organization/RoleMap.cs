﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SSRS.PlugIns.Organization;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 
    
    /// 创建人：上善若水

    /// 角色管理
    /// </summary>
    public class RoleMap : IEntityTypeConfiguration<RoleEntity>
    {
 
        public void Configure(EntityTypeBuilder<RoleEntity> builder)
        {
             builder.HasKey(p => p.F_RoleId);
        }
    }
}
