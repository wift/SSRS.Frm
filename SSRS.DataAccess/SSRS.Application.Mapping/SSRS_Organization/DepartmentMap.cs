﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SSRS.PlugIns.Organization;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 
    
    /// 创建人：上善若水

    /// 部门管理
    /// </summary>
    public class DepartmentMap : IEntityTypeConfiguration<DepartmentEntity>
    {
 

        public void Configure(EntityTypeBuilder<DepartmentEntity> builder)
        {
             builder.HasKey(p => p.F_DepartmentId);
        }
    }
}
