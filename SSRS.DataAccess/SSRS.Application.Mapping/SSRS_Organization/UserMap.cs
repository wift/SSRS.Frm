﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SSRS.PlugIns.Organization;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 
    
    /// 创建人：上善若水

    /// 用户数据库实体映射
    /// </summary>
    public class UserMap : IEntityTypeConfiguration<UserEntity>
    {
 
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
             builder.HasKey(p => p.F_UserId);
        }
    }
}
