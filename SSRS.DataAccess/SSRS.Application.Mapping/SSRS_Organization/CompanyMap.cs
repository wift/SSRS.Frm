﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SSRS.PlugIns.Organization;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 
    
    /// 创建人：上善若水

    /// 机构管理
    /// </summary>
    public class CompanyMap : IEntityTypeConfiguration<CompanyEntity>
    { 

        public void Configure(EntityTypeBuilder<CompanyEntity> builder)
        {
             builder.HasKey(p => p.F_CompanyId);
        }
    }
}
