﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SSRS.PlugIns.Base.AuthorizeModule;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 
    
    /// 创建人：上善若水
    /// 日 期：2017.04.17
    /// 用户关联对象
    /// </summary>
    public class UserRelationMap : IEntityTypeConfiguration<UserRelationEntity>
    {
 
        public void Configure(EntityTypeBuilder<UserRelationEntity> builder)
        {
             builder.HasKey(p => p.F_UserId);
        }
    }
}
