﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SSRS.PlugIns.Base.AuthorizeModule;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 
    
    /// 创建人：上善若水
    /// 日 期：2017.04.17
    /// 授权功能
    /// </summary>
    public class AuthorizeMap : IEntityTypeConfiguration<AuthorizeEntity>
    {
       

        public void Configure(EntityTypeBuilder<AuthorizeEntity> builder)
        {
             builder.HasKey(p => p.F_AuthorizeId);
        }
    }
}
