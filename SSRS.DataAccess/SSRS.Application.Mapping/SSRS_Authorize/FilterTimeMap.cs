﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SSRS.PlugIns.Base.AuthorizeModule;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 
    
    /// 创建人：上善若水
    /// 日 期：2017.04.17
    /// 时段过滤
    /// </summary>
    public class FilterTimeMap : IEntityTypeConfiguration<FilterTimeEntity>
    {
 
        public void Configure(EntityTypeBuilder<FilterTimeEntity> builder)
        {
             builder.HasKey(p => p.F_FilterTimeId);
        }
    }
}
