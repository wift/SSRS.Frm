﻿using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.FileProviders;

namespace SSRS.Core
{
    public class SystemConfig : IConfig
    {
        public SystemConfig()
        {
        }


        public string EnvironmentName { get; set; }
        public string ApplicationName { get; set; }
        public string WebRootPath { get; set; }
        public IFileProvider WebRootFileProvider { get; set; }
        public string ContentRootPath { get; set; }
        public IFileProvider ContentRootFileProvider { get; set; }
    }
}
