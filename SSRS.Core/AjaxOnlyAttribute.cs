﻿using Microsoft.AspNetCore.Mvc.ActionConstraints;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc.Abstractions;

namespace SSRS.Core
{
    [AttributeUsage(AttributeTargets.Method)]
    public class AjaxOnlyAttribute : ActionMethodSelectorAttribute
    {
        /// <summary>
        /// 初始化仅允许Ajax操作
        /// </summary>
        /// <param name="ignore">跳过Ajax检测</param>
        public AjaxOnlyAttribute(bool ignore = false)
        {
            Ignore = ignore;
        }

        /// <summary>
        /// 跳过Ajax检测
        /// </summary>
        public bool Ignore { get; set; }

        public override bool IsValidForRequest(RouteContext routeContext,ActionDescriptor action)
        {
            if (Ignore)
                return true;
            var requestedWith = routeContext.HttpContext.Request.Headers["X-Requested-With"];
            string method = routeContext.HttpContext.Request.Method;

            if (method.ToUpper() == "POST" || method.ToUpper() == "GET")
            {
                if (requestedWith == "XMLHttpRequest")
                {
                    return true;
                }
            }

            return false;
        }
    }
}
