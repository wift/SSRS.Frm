# SSRS.Frm 极限开发框架

## NetCore2.2介绍

###分层编译

分层编译特性使得运行时可以更灵活地使用JIT编译器，从而提升在启动方面和最大化吞吐量上的性能。该特性在.NET Core 2.1时已作为可选特性加入其中，在.NET Core 2.2的预览版本中被默认使用，而到了最终发布阶段，还是改回了可选特性。.NET Core 3.0之后或许会成为默认选项吧。

##平台支持
###NET Core 2.2支持下列操作系统：

- Windows Client: 7, 8.1, 10 (1607+)
- Windows Server: 2008 R2 SP1+
- macOS: 10.12+
- RHEL: 6+
- Fedora: 26+
- Ubuntu: 16.04+
- Debian: 9+
- SLES: 12+
- openSUSE: 42.3+

###芯片支持包括：

- x64 on Windows, macOS, and Linux
- x86 on Windows
- ARM32 on Linux (Ubuntu 16.04+, Debian 9+)
- ARM32 on Windows (1809+; available in January)

##ASP.NET Core 2.2
- 与Swagger类库更好地集成，代码分析提供了设计时检查。
- 引入终端路由，提升了MVC中20%的路由性能
- 改进LinkGenerator的URL生成，支持路由参数转换
- 加入了对于应用程序的健康进行监测新API(此功能来源自和BeatPulse项目的集成)
- 由于进程内托管的支持，在IIS上提升了400%的吞吐量
- 提升了15%的MVC模型验证性能
- 在MVC中加入了对问题明细(Problem Details)(RFC 7807)的支持
- 在ASP.NET Core中可以预览对HTTP/2 server的支持
- 对于Bootstrap 4与Angular 6模板的升级
- 提供了ASP.NET Core SignalR的Java客户端
- 在Linux上提升了60%的HTTP客户端性能，同时在Windows上提升了20%

#### 极限开发框架 技术一览表
 
序号 | 技术选型 |  极限开发框架  |是否集成
 :-----:| :----: | :----: | :----: 
1 | 核心框架 | ASP.Net Core 2.2 MVC |是
2 | 视图框架 | LayuiAdmin v1.2.1 |否
3 | 报表    | ECharts|否
4 | 任务调度 | Quartz.NET|否
5 | 缓存框架 | redis 3.0+|否
6 | 日志管理 | Log4Net|否
7 | Js框架 |  JQuery、Layui|否
8 | Css框架 | Layui|否
9 | 富文本	 | UEditorNetCore|否
10 | 手机端框架 | HTML5 、LayuiMobile|否
11 | 数据表格 |  LayuiTable、Ag-Grid|否
12 | 日期控件 | LayDate.js|否
13 | 开发工具 | VS2017 、Vs Code|否
14 | 容器 | Docker|否
15 | 日期控件 | LayDate.js|否
16 | 即时通讯 | LayIM |否
#### 极限开发框架子系统模块

- 权限模块（组织机构、功能授权、数据授权、其他授权）
- 支付模块（支付宝、微信、美团）
- 大数据分析模块
- 报表模块
- 任务调度子系统
- 多数据源
- 消息中心
- 邮件服务
- 等



## 极限敏捷开发框架 QQ交流群：敏捷开发框架顶层设计(215250420) 