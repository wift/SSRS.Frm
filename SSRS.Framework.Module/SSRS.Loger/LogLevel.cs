﻿using System.ComponentModel;
namespace SSRS.Loger
{
    /// <summary>
    /// 版 本 SSRS-ADMS V1.0.0
    /// Copyright (c) 2013-2018 上善若水
    /// 创建人：上善若水
    /// 日志级别
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// 错误
        /// </summary>
        [Description("错误")]
        Error,
        /// <summary>
        /// 警告
        /// </summary>
        [Description("警告")]
        Warning,
        /// <summary>
        /// 信息
        /// </summary>
        [Description("信息")]
        Info,
        /// <summary>
        /// 调试
        /// </summary>
        [Description("调试")]
        Debug
    }
}
