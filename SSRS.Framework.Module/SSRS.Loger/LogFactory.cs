﻿using log4net;
using System;
using log4net.Repository;
using log4net.Repository.Hierarchy;

namespace SSRS.Loger
{
    /// <summary>
    /// 版 本 SSRS-ADMS V1.0.0
    /// Copyright (c) 2013-2018 上善若水
    /// 创建人：上善若水
    /// redis操作方法
    /// </summary>
    public class LogFactory
    {
        private static readonly ILoggerRepository Repository = LogManager.CreateRepository("NETCoreRepository");
        /// <summary>
        /// 构造函数
        /// </summary>
        static LogFactory()
        {
            log4net.Config.XmlConfigurator.Configure(Repository);
        }
        /// <summary>
        /// 获取日志操作对象
        /// </summary>
        /// <param name="type">类型</param>
        /// <returns></returns>
        public static Log GetLogger(Type type)
        {
            return new Log(LogManager.GetLogger(type));
        }

        /// <summary>
        /// 获取日志操作对象
        /// </summary> 
        /// <param name="name">名字</param>
        /// <returns></returns>
        public static Log GetLogger(string name) => new Log(LogManager.GetLogger(Repository.Name, name));
    }
}
