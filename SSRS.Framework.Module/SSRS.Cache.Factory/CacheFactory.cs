﻿using SSRS.Cache.Base;
using SSRS.Cache.Redis;

namespace SSRS.Cache.Factory
{
    /// <summary>
    /// 定义缓存工厂类
    /// </summary>
    public class CacheFactory
    {
        public static ICache CaChe()
        {
            return new CacheByRedis();
        }
    }
}
