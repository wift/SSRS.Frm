﻿namespace SSRS.Util
{
    /// <summary>
    /// 版 本  SSRS-ADMS V1.0.0 红云敏捷开发框架
    /// Copyright (c) 2013-2018 上善若水
    /// 创建人：上善若水
    /// 日 期：2017.03.08
    /// 接口响应数据
    /// </summary>
    public class ResParameter
    {
        /// <summary>
        /// 接口响应码
        /// </summary>
        public ResponseCode code { get; set; }
        /// <summary>
        /// 接口响应消息
        /// </summary>
        public string info { get; set; }
        /// <summary>
        /// 接口响应数据
        /// </summary>
        public object data { get; set; }
    }
}
