﻿using System.Web;
using SSRS.Cache.Base;
using SSRS.Cache.Factory;

namespace SSRS.Util
{
    /// <summary>
    /// 版 本  SSRS-ADMS V1.0.0 红云敏捷开发框架
    /// Copyright (c) 2013-2018 上善若水
    /// 创建人：上善若水
    /// 日 期：2017.03.08
    /// 当前上下文执行用户信息获取
    /// </summary>
    public static class LoginUserInfo
    {
        private static ICache redisCache = CacheFactory.CaChe();
        /// <summary>
        /// 获取当前上下文执行用户信息
        /// </summary>
        /// <returns></returns>
        public static UserInfo Get()
        {
            return (UserInfo)HttpContext.Current.Items["LoginUserInfo"];
        }
    }
}
