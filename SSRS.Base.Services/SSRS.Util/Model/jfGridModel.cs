﻿namespace SSRS.Util
{
    /// <summary>
    /// 版 本  SSRS-ADMS V1.0.0 红云敏捷开发框架
    /// Copyright (c) 2013-2018 上善若水
    /// 创建人：上善若水
    /// 日 期：2017.07.10
    /// 表格属性模型
    /// </summary>
    public class jfGridModel
    {
        public string name { get; set; }
        public string label { get; set; }
        public string width { get; set; }
        public string align { get; set; }
        public string height { get; set; }
        public string hidden { get; set; }
    }
}
