﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSRS.Util.Ueditor
{
    /// <summary>
    /// 版 本  SSRS-ADMS V1.0.0 红云敏捷开发框架
    /// Copyright (c) 2013-2018 上善若水
    /// 创建人：上善若水
    /// 日 期：2019.04.07
    /// 百度编辑器UE上传返回结果
    /// </summary>
    public class UeditorUploadResult
    {
        public UeditorUploadState State { get; set; }
        public string Url { get; set; }
        public string OriginFileName { get; set; }

        public string ErrorMessage { get; set; }
    }

    public enum UeditorUploadState
    {
        Success = 0,
        SizeLimitExceed = -1,
        TypeNotAllow = -2,
        FileAccessError = -3,
        NetworkError = -4,
        Unknown = 1,
    }
}
