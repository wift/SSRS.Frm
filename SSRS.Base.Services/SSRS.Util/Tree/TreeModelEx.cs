﻿
using System.Collections.Generic;
namespace SSRS.Util
{
    /// <summary>
    /// 版 本  SSRS-ADMS V1.0.0 红云敏捷开发框架
    /// Copyright (c) 2013-2018 上善若水
    /// 创建人：上善若水
    /// 日 期：2019.04.07
    /// 树结构数据
    /// </summary>
    public class TreeModelEx<T> where T : class
    {
        /// <summary>
        /// 节点id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 父级节点ID
        /// </summary>
        public string parentId { get; set; }
        /// <summary>
        /// 数据
        /// </summary>
        public T data { get; set; }
        /// <summary>
        /// 子节点列表数据
        /// </summary>
        public List<TreeModelEx<T>> ChildNodes { get; set; }
        public List<TreeModelEx<T>> children { get; set; }

    }
}
