﻿
namespace SSRS.Util
{
    /// <summary>
    /// 版 本  SSRS-ADMS V1.0.0 红云敏捷开发框架
    /// Copyright (c) 2013-2018 上善若水
    /// 创建人：上善若水
    /// 日 期：2017.03.04
    /// 模板数据模型
    /// </summary>
    public class TemplateDataModel
    {
        /// <summary>
        /// 行号
        /// </summary>
        public int row { get; set; }
        /// <summary>
        /// 列号
        /// </summary>
        public int cell { get; set; }
        /// <summary>
        /// 数据值
        /// </summary>
        public string value { get; set; }
    }
}
