﻿namespace SSRS.PlugIns.Base.SystemModule
{
    /// <summary>
    /// 
    
    /// 创建人：上善若水
    /// 日 期：2018.03.27
    /// 数据库连接数据模型
    /// </summary>
    public class DatabaseLinkModel
    {
        /// <summary>
        /// 别名
        /// </summary>
        public string alias { get; set; }
        /// <summary>
        /// 名字
        /// </summary>
        public string name { get; set; }
    }
}
